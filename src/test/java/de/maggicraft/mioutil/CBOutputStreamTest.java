package de.maggicraft.mioutil;

import de.maggicraft.mioutil.cbstream.CBInputStream;
import de.maggicraft.mioutil.cbstream.CBOutputStream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

/**
 * Test die Grundfunktionen der {@code CBStream}-Klassen. Dazu zählen das Schreiben, lesen und
 * schließen der Streams.
 * <p>
 * Created by Marc Schmidt on 08.04.2017.
 */
@SuppressWarnings("LocalVariableOfConcreteClass")
final class CBOutputStreamTest {

   /**
    * Testet ob alle Chars geschrieben und richtig gelesen werden können.
    */
   @Test
   void testCBWrite(@TempDir Path pTempDir) throws IOException {
      Path file = pTempDir.resolve("cb_write_test.txt");

      int length = Character.MAX_VALUE;
      StringBuilder sb = new StringBuilder(length);
      for (int i = 0; i < Character.MAX_VALUE; i++) {
         sb.append((char) i);
      }

      try (CBOutputStream out = new CBOutputStream(file)) {
         out.writeChars(sb.toString());
      }

      try (CBInputStream in = new CBInputStream(file)) {
         for (int i = 0; i < Character.MAX_VALUE; i++) {
            Assertions.assertEquals(i, in.readChar());
         }
      }
   }

   /**
    * Testet ob eine zufällige Aneinanderreihung von Chars geschrieben und richtig gelesen werden
    * kann.
    */
   @Test
   void testCBWriteRandom(@TempDir Path pTempDir) throws IOException {
      Path file = pTempDir.resolve("cb_random_write_test.txt");

      Random rdm = new Random();
      int capacity = 3 * Character.MAX_VALUE;
      StringBuilder sb = new StringBuilder(capacity);
      for (int i = 0; i < capacity; i++) {
         sb.append((char) rdm.nextInt(Character.MAX_VALUE));
      }
      String str = sb.toString();

      try (CBOutputStream out = new CBOutputStream(file)) {
         out.writeChars(str);
      }

      try (CBInputStream in = new CBInputStream(file)) {
         for (int i = 0; i < Character.MAX_VALUE; i++) {
            Assertions.assertEquals(str.charAt(i), in.readChar());
         }
      }
   }

   /**
    * Testet ob der {@link CBOutputStream} geschlossen werden kann.
    */
   @Test
   void testCBWriteClose(@TempDir Path pTempDir) throws IOException {
      Path file = pTempDir.resolve("cb_write_close_test.txt");

      try (CBOutputStream out = new CBOutputStream(file)) {
         out.writeChars("test");
      }
      Assertions.assertTrue(Files.deleteIfExists(file));
   }

   /**
    * Testet ob der {@link CBInputStream} geschlossen werden kann.
    */
   @Test
   void testCBReadClose(@TempDir Path pTempDir) throws IOException {
      Path file = pTempDir.resolve("cb_write_read_close_test.txt");
      try (CBOutputStream out = new CBOutputStream(file)) {
         out.writeChars("test");
      }

      try (CBInputStream in = new CBInputStream(file)) {
         int a = in.readChar();
      }

      Assertions.assertTrue(Files.deleteIfExists(file));
   }
}
