package de.maggicraft.mioutil;

import de.maggicraft.mioutil.compr.Compressed;
import de.maggicraft.mtest.TestUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * created on 2019.03.03 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
@SuppressWarnings("ALL")
final class CompressionTest {

   @RepeatedTest(4)
   void testInit() {
      Assertions.assertAll(() -> {
         int units = TestUtil.rdm(10000);
         int[] arr = TestUtil.rdmIntArray(units, 1000);

         Compressed compressed = new Compressed(arr);

         Assertions.assertEquals(units, compressed.size(), "should be the same length");
         for (int unit = 0; unit < units; unit++) {
            String message = "not same for unit " + unit;
            Assertions.assertEquals(arr[unit], compressed.get(unit), message);
         }
      });
   }

   @ParameterizedTest
   @ValueSource(
         ints = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
               24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34})
   void testInitBits(int pBits) {
      int units = TestUtil.rdm(10000);
      int[] arr = TestUtil.rdmIntArray(units, (int) StrictMath.pow(2, pBits));

      Compressed compressed = new Compressed(units, pBits);
      for (int num = 0; num < compressed.size(); num++) {
         compressed.set(num, arr[num]);
      }

      Assertions.assertEquals(units, compressed.size(), "should be the same length");
      Assertions.assertEquals(pBits, compressed.getBits(), "bits should be the same");
      for (int unit = 0; unit < units; unit++) {
         String message = "not same value for unit " + unit;
         Assertions.assertEquals(arr[unit], compressed.get(unit), message);
      }
   }
}
