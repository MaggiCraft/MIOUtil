package de.maggicraft.mioutil;

import de.maggicraft.mioutil.json.IUniqueID;
import de.maggicraft.mtest.IEnumSupplierTestCase;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.HashSet;

/**
 * created on 2019.05.11 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface IUniqueTestCase<E extends IUniqueID<U>, U> extends IEnumSupplierTestCase<E, U> {

   @Test
   default void testUnique() {
      E[] items = getEnumItems();
      Collection<U> set = new HashSet<>(items.length);

      for (E item : items) {
         U unique = getValue(item);

         String message = "the value \"" + unique + "\" of the enum property \"" + item +
                          "\" is contained twice";
         Assertions.assertFalse(set.contains(unique), message);

         set.add(unique);
      }
   }

   /**
    * Tests if the properties returned by
    * {@link de.maggicraft.mtest.IUniqueAttributeTestCase#getEnumItems()}
    * are unique.
    */
   @Test
   default void testNotNull() {
      E[] items = getEnumItems();

      for (E item : items) {
         U value = getValue(item);
         String message = "the value \"" + value + "\" of the enum property \"" + item +
                          "\" must not bet null";
         Assertions.assertNotNull(value, message);
      }
   }

   @NotNull
   @Override
   default U getValue(E pItem) {
      return pItem.getUID();
   }
}
