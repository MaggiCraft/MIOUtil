/*
 * The Apache Software License, Version 1.1 Copyright (c) 2001-2003 The Apache Software Foundation.
 * All rights reserved. Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 1. Redistributions of
 * source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer. 2. Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution. 3. The end-user documentation included with the redistribution,
 * if any, must include the following acknowledgement: "This product includes software developed by
 * the Apache Software Foundation (http://www.apache.org/)." Alternately, this acknowledgement may
 * appear in the software itself, if and wherever such third-party acknowledgements normally appear.
 * 4. The names "Ant" and "Apache Software Foundation" must not be used to endorse or promote
 * products derived from this software without prior written permission. For written permission,
 * please contact apache@apache.org. 5. Products derived from this software may not be called
 * "Apache" nor may "Apache" appear in their names without prior written permission of the Apache
 * Group. THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR ITS CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ==================================================================== This software consists of
 * voluntary contributions made by many individuals on behalf of the Apache Software Foundation. For
 * more information on the Apache Software Foundation, please see <http://www.apache.org/>.
 */

/*
 * This package is based on the work done by Keiron Liddle, Aftex Software <keiron@aftexsw.com>
 to whom the Ant project
 * is very grateful for his great code.
 */
package de.maggicraft.mioutil.cbstream;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * An input stream that decompresses from the BZip2 format (without the file header chars) to be
 * read as any other stream.
 *
 * @author <a href="mailto:keiron@aftexsw.com">Keiron Liddle</a>
 */
@SuppressWarnings(
      {"OverlyComplexMethod", "CatchMayIgnoreException", "InstanceVariableOfConcreteClass",
            "ClassWithTooManyFields", "OverlyComplexClass", "OverlyLongMethod",
            "OverlyNestedMethod"})
public final class CBInputStream extends InputStream {

   private static final int START_BLOCK_STATE = 1;
   private static final int RAND_PART_A_STATE = 2;
   private static final int RAND_PART_B_STATE = 3;
   private static final int RAND_PART_C_STATE = 4;
   private static final int NO_RAND_PART_A_STATE = 5;
   private static final int NO_RAND_PART_B_STATE = 6;
   private static final int NO_RAND_PART_C_STATE = 7;
   @NotNull
   private final CBMap mCrc = new CBMap();
   @NotNull
   private final int[][] mLimit = new int[CBConstants.N_GROUPS][CBConstants.MAX_ALPHA_SIZE];
   @NotNull
   private final int[][] mBase = new int[CBConstants.N_GROUPS][CBConstants.MAX_ALPHA_SIZE];
   @NotNull
   private final int[][] mPerm = new int[CBConstants.N_GROUPS][CBConstants.MAX_ALPHA_SIZE];
   @NotNull
   private final int[] mUnzftab = new int[256];
   @NotNull
   private final int[] mMinLens = new int[CBConstants.N_GROUPS];
   @NotNull
   private final char[] mSeqToUnseq = new char[256];
   @NotNull
   private final char[] mSelector = new char[CBConstants.MAX_SELECTORS];
   @NotNull
   private final char[] mSelectorMtf = new char[CBConstants.MAX_SELECTORS];
   @NotNull
   private final boolean[] mInUse = new boolean[256];
   private InputStream mBsStream;
   private int[] mTt;
   private int mNInUse;
   private int mCurrentChar = -1;
   private int mBlockState = START_BLOCK_STATE;
   private int mComputedCombinedCRC;
   private int mI2;
   private int mCount;
   private int mChPrev;
   private int mCh2;
   private int mTPos;
   private int mRNToGo;
   private int mRTPos;
   private int mJ2;
   private int mLast;
   private int mOrigPtr;
   private int mBsBuff;
   private int mBsLive;
   private char[] mLl8;
   private char mCharZ;
   private boolean mStreamEnd;
   private boolean mBlockRandomised;

   public CBInputStream(@NotNull Path pPath) throws IOException {
      this(Files.newInputStream(pPath));
   }

   public CBInputStream(@NotNull File pFile) throws FileNotFoundException {
      this(new FileInputStream(pFile));
   }

   public CBInputStream(InputStream pStream) {
      mLl8 = null;
      mTt = null;
      bsSetStream(pStream);
      initialize();
      initBlock();
      setupBlock();
   }

   private static void hbCreateDecodeTables(int[] pLimit, int[] pBase, int[] pPerm, char[] pLength,
         int pMinLen, int pMaxLen, int pAlphaSize) {
      int pp, i, vec;

      pp = 0;
      for (i = pMinLen; i <= pMaxLen; i++) {
         for (int j = 0; j < pAlphaSize; j++) {
            if (pLength[j] == i) {
               pPerm[pp] = j;
               pp++;
            }
         }
      }

      for (i = 0; i < CBConstants.MAX_CODE_LEN; i++) {
         pBase[i] = 0;
      }
      for (i = 0; i < pAlphaSize; i++) {
         pBase[pLength[i] + 1]++;
      }

      for (i = 1; i < CBConstants.MAX_CODE_LEN; i++) {
         pBase[i] += pBase[i - 1];
      }

      for (i = 0; i < CBConstants.MAX_CODE_LEN; i++) {
         pLimit[i] = 0;
      }
      vec = 0;

      for (i = pMinLen; i <= pMaxLen; i++) {
         vec += pBase[i + 1] - pBase[i];
         pLimit[i] = vec - 1;
         vec <<= 1;
      }
      for (i = pMinLen + 1; i <= pMaxLen; i++) {
         pBase[i] = ((pLimit[i - 1] + 1) << 1) - pBase[i];
      }
   }

   @Contract(pure = true)
   @Override
   public @NotNull String toString() {
      return "CBInputStream{}";
   }

   public final char readChar() throws EOFException {
      int ch1 = read();
      int ch2 = read();
      if ((ch1 | ch2) < 0) {
         throw new EOFException("no char left");
      }
      return (char) ((ch1 << 8) + ch2);
   }

   public final int readInt() throws IOException {
      int ch1 = read();
      int ch2 = read();
      int ch3 = read();
      int ch4 = read();
      if ((ch1 | ch2 | ch3 | ch4) < 0) {
         throw new EOFException("no int left");
      }
      return (ch1 << 24) + (ch2 << 16) + (ch3 << 8) + ch4;
   }

   @SuppressWarnings("ReturnSeparatedFromComputation")
   @Override
   public int read() {
      if (mStreamEnd) {
         return -1;
      } else {
         int retChar = mCurrentChar;
         switch (mBlockState) {
            case RAND_PART_B_STATE:
               setupRandPartB();
               break;
            case RAND_PART_C_STATE:
               setupRandPartC();
               break;
            case NO_RAND_PART_B_STATE:
               setupNoRandPartB();
               break;
            case NO_RAND_PART_C_STATE:
               setupNoRandPartC();
               break;
            default:
               break;
         }
         return retChar;
      }
   }

   @Override
   public void close() throws IOException {
      if (mBsStream != null) {
         mBsStream.close();
      }
   }

   private void makeMaps() {
      mNInUse = 0;
      for (int i = 0; i < 256; i++) {
         if (mInUse[i]) {
            mSeqToUnseq[mNInUse] = (char) i;
            mNInUse++;
         }
      }
   }

   private void initialize() {
      char magic3, magic4;
      magic3 = bsGetUChar();
      magic4 = bsGetUChar();
      if ((magic3 != 'h') || (magic4 < '1') || (magic4 > '9')) {
         bsFinishedWithStream();
         mStreamEnd = true;
         return;
      }

      setDecompressStructureSizes(magic4 - '0');
      mComputedCombinedCRC = 0;
   }

   private void initBlock() {
      char magic1, magic2, magic3, magic4;
      char magic5, magic6;
      magic1 = bsGetUChar();
      magic2 = bsGetUChar();
      magic3 = bsGetUChar();
      magic4 = bsGetUChar();
      magic5 = bsGetUChar();
      magic6 = bsGetUChar();
      if ((magic1 == 0x17) && (magic2 == 0x72) && (magic3 == 0x45) && (magic4 == 0x38) &&
          (magic5 == 0x50) && (magic6 == 0x90)) {
         complete();
         return;
      }

      if ((magic1 != 0x31) || (magic2 != 0x41) || (magic3 != 0x59) || (magic4 != 0x26) ||
          (magic5 != 0x53) || (magic6 != 0x59)) {
         mStreamEnd = true;
         return;
      }

      bsGetInt32();

      mBlockRandomised = bsR(1) == 1;

      getAndMoveToFrontDecode();

      mCrc.initialiseCRC();
      mBlockState = START_BLOCK_STATE;
   }

   private void endBlock() {
      int computedBlockCRC = mCrc.getFinalCRC();

      mComputedCombinedCRC = (mComputedCombinedCRC << 1) | (mComputedCombinedCRC >>> 31);
      mComputedCombinedCRC ^= computedBlockCRC;
   }

   private void complete() {
      bsGetInt32();

      bsFinishedWithStream();
      mStreamEnd = true;
   }

   private void bsFinishedWithStream() {
      try {
         if (mBsStream != null) {
            if (mBsStream != System.in) {
               mBsStream.close();
               mBsStream = null;
            }
         }
      } catch (IOException ioe) {
      }
   }

   @Contract(mutates = "this")
   private void bsSetStream(InputStream pInputStream) {
      mBsStream = pInputStream;
      mBsLive = 0;
      mBsBuff = 0;
   }

   private int bsR(int pNum) {
      int v;
      while (mBsLive < pNum) {
         int zzi;
         char thech = 0;
         try {
            thech = (char) mBsStream.read();
         } catch (IOException pE) {
         }
         zzi = thech;
         mBsBuff = (mBsBuff << 8) | (zzi & 0xff);
         mBsLive += 8;
      }

      v = (mBsBuff >> (mBsLive - pNum)) & ((1 << pNum) - 1);
      mBsLive -= pNum;
      return v;
   }

   private char bsGetUChar() {
      return (char) bsR(8);
   }

   private void bsGetint() {
      bsR(8);
      bsR(8);
      bsR(8);
      bsR(8);
   }

   private void bsGetInt32() {
      bsGetint();
   }

   private void recvDecodingTables() {
      char[][] len = new char[CBConstants.N_GROUPS][CBConstants.MAX_ALPHA_SIZE];
      int i, j, t, nGroups, nSelectors, alphaSize;
      boolean[] inUse16 = new boolean[16];

      for (i = 0; i < 16; i++) {
         inUse16[i] = bsR(1) == 1;
      }

      for (i = 0; i < 256; i++) {
         mInUse[i] = false;
      }

      for (i = 0; i < 16; i++) {
         if (inUse16[i]) {
            for (j = 0; j < 16; j++) {
               if (bsR(1) == 1) {
                  mInUse[(i << 4) + j] = true;
               }
            }
         }
      }

      makeMaps();
      alphaSize = mNInUse + 2;

      nGroups = bsR(3);
      nSelectors = bsR(15);
      for (i = 0; i < nSelectors; i++) {
         j = 0;
         while (bsR(1) == 1) {
            j++;
         }
         mSelectorMtf[i] = (char) j;
      }

      char[] pos = new char[CBConstants.N_GROUPS];
      char v;
      for (v = 0; v < nGroups; v++) {
         pos[v] = v;
      }

      for (i = 0; i < nSelectors; i++) {
         v = mSelectorMtf[i];
         char tmp = pos[v];
         while (v > 0) {
            pos[v] = pos[v - 1];
            v--;
         }
         pos[0] = tmp;
         mSelector[i] = tmp;
      }

      for (t = 0; t < nGroups; t++) {
         int curr = bsR(5);
         for (i = 0; i < alphaSize; i++) {
            while (bsR(1) == 1) {
               if (bsR(1) == 0) {
                  curr++;
               } else {
                  curr--;
               }
            }
            len[t][i] = (char) curr;
         }
      }

      for (t = 0; t < nGroups; t++) {
         int minLen = 32;
         int maxLen = 0;
         for (i = 0; i < alphaSize; i++) {
            if (len[t][i] > maxLen) {
               maxLen = len[t][i];
            }
            if (len[t][i] < minLen) {
               minLen = len[t][i];
            }
         }
         hbCreateDecodeTables(mLimit[t], mBase[t], mPerm[t], len[t], minLen, maxLen, alphaSize);
         mMinLens[t] = minLen;
      }
   }

   private void getAndMoveToFrontDecode() {
      char[] yy = new char[256];
      int i, nextSym;
      int EOB, groupNo, groupPos;

      mOrigPtr = bsR(24);

      recvDecodingTables();
      EOB = mNInUse + 1;
      groupNo = -1;

      for (i = 0; i <= 255; i++) {
         mUnzftab[i] = 0;
      }

      for (i = 0; i <= 255; i++) {
         yy[i] = (char) i;
      }

      mLast = -1;

      {
         int zt, zn, zvec;
         groupNo++;
         groupPos = CBConstants.G_SIZE;
         groupPos--;
         zt = mSelector[groupNo];
         zn = mMinLens[zt];
         zvec = bsR(zn);
         while (zvec > mLimit[zt][zn]) {
            zn++;
            while (mBsLive < 1) {
               int zzi;
               char thech = 0;
               try {
                  thech = (char) mBsStream.read();
               } catch (IOException e) {
               }
               zzi = thech;
               mBsBuff = (mBsBuff << 8) | (zzi & 0xff);
               mBsLive += 8;
            }
            int zj = (mBsBuff >> (mBsLive - 1)) & 1;
            mBsLive--;
            zvec = (zvec << 1) | zj;
         }
         nextSym = mPerm[zt][zvec - mBase[zt][zn]];
      }

      while (nextSym != EOB) {
         if ((nextSym == CBConstants.RUNA) || (nextSym == CBConstants.RUNB)) {
            char ch;
            int i1 = -1;
            int N = 1;
            do {
               if (nextSym == CBConstants.RUNA) {
                  i1 += N;
               } else {
                  i1 += (1 + 1) * N;
               }
               N <<= 1;
               int zt, zn, zvec;
               if (groupPos == 0) {
                  groupNo++;
                  groupPos = CBConstants.G_SIZE;
               }
               groupPos--;
               zt = mSelector[groupNo];
               zn = mMinLens[zt];
               zvec = bsR(zn);
               while (zvec > mLimit[zt][zn]) {
                  zn++;
                  while (mBsLive < 1) {
                     int zzi;
                     char thech = 0;
                     try {
                        thech = (char) mBsStream.read();
                     } catch (IOException pE) {
                     }
                     zzi = thech;
                     mBsBuff = (mBsBuff << 8) | (zzi & 0xff);
                     mBsLive += 8;
                  }
                  int zj = (mBsBuff >> (mBsLive - 1)) & 1;
                  mBsLive--;
                  zvec = (zvec << 1) | zj;
               }
               nextSym = mPerm[zt][zvec - mBase[zt][zn]];
            } while ((nextSym == CBConstants.RUNA) || (nextSym == CBConstants.RUNB));

            i1++;
            ch = mSeqToUnseq[yy[0]];
            mUnzftab[ch] += i1;

            while (i1 > 0) {
               mLast++;
               mLl8[mLast] = ch;
               i1--;
            }

         } else {
            char tmp;
            mLast++;

            tmp = yy[nextSym - 1];
            mUnzftab[mSeqToUnseq[tmp]]++;
            mLl8[mLast] = mSeqToUnseq[tmp];

            int j = nextSym - 1;
            for (; j > 3; j -= 4) {
               yy[j] = yy[j - 1];
               yy[j - 1] = yy[j - 2];
               yy[j - 2] = yy[j - 3];
               yy[j - 3] = yy[j - 4];
            }
            for (; j > 0; j--) {
               yy[j] = yy[j - 1];
            }

            yy[0] = tmp;
            int zt, zn, zvec;
            if (groupPos == 0) {
               groupNo++;
               groupPos = CBConstants.G_SIZE;
            }
            groupPos--;
            zt = mSelector[groupNo];
            zn = mMinLens[zt];
            zvec = bsR(zn);
            while (zvec > mLimit[zt][zn]) {
               zn++;
               while (mBsLive < 1) {
                  int zzi;
                  char thech = 0;
                  try {
                     thech = (char) mBsStream.read();
                  } catch (IOException e) {
                  }
                  zzi = thech;
                  mBsBuff = (mBsBuff << 8) | (zzi & 0xff);
                  mBsLive += 8;
               }
               int zj = (mBsBuff >> (mBsLive - 1)) & 1;
               mBsLive--;
               zvec = (zvec << 1) | zj;
            }
            nextSym = mPerm[zt][zvec - mBase[zt][zn]];
         }
      }
   }

   private void setupBlock() {
      int[] cftab = new int[257];

      cftab[0] = 0;
      int i;
      for (i = 1; i <= 256; i++) {
         cftab[i] = mUnzftab[i - 1];
      }
      for (i = 1; i <= 256; i++) {
         cftab[i] += cftab[i - 1];
      }

      for (i = 0; i <= mLast; i++) {
         char ch = mLl8[i];
         mTt[cftab[ch]] = i;
         cftab[ch]++;
      }

      mTPos = mTt[mOrigPtr];

      mCount = 0;
      mI2 = 0;
      mCh2 = 256;

      if (mBlockRandomised) {
         mRNToGo = 0;
         mRTPos = 0;
         setupRandPartA();
      } else {
         setupNoRandPartA();
      }
   }

   private void setupRandPartA() {
      if (mI2 > mLast) {
         endBlock();
         initBlock();
         setupBlock();
      } else {
         mChPrev = mCh2;
         mCh2 = mLl8[mTPos];
         mTPos = mTt[mTPos];
         if (mRNToGo == 0) {
            mRNToGo = CBConstants.R_NUMS[mRTPos];
            mRTPos++;
            if (mRTPos == 512) {
               mRTPos = 0;
            }
         }
         mRNToGo--;
         mCh2 ^= (mRNToGo == 1) ? 1 : 0;
         mI2++;

         mCurrentChar = mCh2;
         mBlockState = RAND_PART_B_STATE;
         mCrc.updateCRC(mCh2);
      }
   }

   private void setupNoRandPartA() {
      if (mI2 <= mLast) {
         mChPrev = mCh2;
         mCh2 = mLl8[mTPos];
         mTPos = mTt[mTPos];
         mI2++;

         mCurrentChar = mCh2;
         mBlockState = NO_RAND_PART_B_STATE;
         mCrc.updateCRC(mCh2);
      } else {
         endBlock();
         initBlock();
         setupBlock();
      }
   }

   private void setupRandPartB() {
      if (mCh2 == mChPrev) {
         mCount++;
         if (mCount >= 4) {
            mCharZ = mLl8[mTPos];
            mTPos = mTt[mTPos];
            if (mRNToGo == 0) {
               mRNToGo = CBConstants.R_NUMS[mRTPos];
               mRTPos++;
               if (mRTPos == 512) {
                  mRTPos = 0;
               }
            }
            mRNToGo--;
            mCharZ ^= (mRNToGo == 1) ? 1 : 0;
            mJ2 = 0;
            mBlockState = RAND_PART_C_STATE;
            setupRandPartC();
         } else {
            mBlockState = RAND_PART_A_STATE;
            setupRandPartA();
         }
      } else {
         mBlockState = RAND_PART_A_STATE;
         mCount = 1;
         setupRandPartA();
      }
   }

   private void setupRandPartC() {
      if (mJ2 < mCharZ) {
         mCurrentChar = mCh2;
         mCrc.updateCRC(mCh2);
         mJ2++;
      } else {
         mBlockState = RAND_PART_A_STATE;
         mI2++;
         mCount = 0;
         setupRandPartA();
      }
   }

   private void setupNoRandPartB() {
      if (mCh2 == mChPrev) {
         mCount++;
         if (mCount >= 4) {
            mCharZ = mLl8[mTPos];
            mTPos = mTt[mTPos];
            mBlockState = NO_RAND_PART_C_STATE;
            mJ2 = 0;
            setupNoRandPartC();
         } else {
            mBlockState = NO_RAND_PART_A_STATE;
            setupNoRandPartA();
         }
      } else {
         mBlockState = NO_RAND_PART_A_STATE;
         mCount = 1;
         setupNoRandPartA();
      }
   }

   private void setupNoRandPartC() {
      if (mJ2 < mCharZ) {
         mCurrentChar = mCh2;
         mCrc.updateCRC(mCh2);
         mJ2++;
      } else {
         mBlockState = NO_RAND_PART_A_STATE;
         mI2++;
         mCount = 0;
         setupNoRandPartA();
      }
   }

   @Contract(mutates = "this")
   private void setDecompressStructureSizes(int pNewSize100K) {
      if (pNewSize100K == 0) {
         return;
      }

      int n = CBConstants.BASE_BLOCK_SIZE * pNewSize100K;
      mLl8 = new char[n];
      mTt = new int[n];
   }

}
