/*
 * The Apache Software License, Version 1.1 Copyright (c) 2001-2003 The Apache Software
 * Foundation. All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 *  provided that the
 * following conditions are met: 1. Redistributions of source code must retain the above
 * copyright notice, this list of
 * conditions and the following disclaimer. 2. Redistributions in binary form must reproduce the
 * above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or other
 * materials provided with the
 * distribution. 3. The end-user documentation included with the redistribution, if any, must
 * include the following
 * acknowledgement: "This product includes software developed by the Apache Software Foundation
 * (http://www.apache.org/)." Alternately, this acknowledgement may appear in the software
 * itself, if and wherever such
 * third-party acknowledgements normally appear. 4. The names "Ant" and "Apache Software
 * Foundation" must not be used to
 * endorse or promote products derived from this software without prior written permission. For
 * written permission,
 * please contact apache@apache.org. 5. Products derived from this software may not be called
 * "Apache" nor may "Apache"
 * appear in their names without prior written permission of the Apache Group. THIS SOFTWARE IS
 * PROVIDED ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE APACHE SOFTWARE
 * FOUNDATION OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * ==================================================================== This software consists of
 *  voluntary
 * contributions made by many individuals on behalf of the Apache Software Foundation. For more
 * information on the
 * Apache Software Foundation, please see <http://www.apache.org/>.
 */

/*
 * This package is based on the work done by Keiron Liddle, Aftex Software <keiron@aftexsw.com>
 to whom the Ant project
 * is very grateful for his great code.
 */

package de.maggicraft.mioutil.cbstream;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

@SuppressWarnings(
      {"OverlyComplexMethod", "InstanceVariableOfConcreteClass", "ClassWithTooManyFields",
            "OverlyComplexClass", "OverlyLongMethod", "OverlyNestedMethod",
            "ClassWithTooManyMethods", "NestedAssignment"})
public final class CBOutputStream extends OutputStream {

   private static final int SETMASK = 1 << 21;
   private static final int CLEARMASK = ~SETMASK;
   private static final int GREATER_ICOST = 15;
   private static final int LESSER_ICOST = 0;
   private static final int SMALL_THRESH = 20;
   private static final int DEPTH_THRESH = 10;
   private static final int QSORT_STACK_SIZE = 1000;

   private final int mBlockSize100k;
   @NotNull
   private final CBMap mCrc = new CBMap();
   @NotNull
   private final boolean[] mInUse = new boolean[256];
   @NotNull
   private final char[] mUnseqToSeq = new char[256];
   @NotNull
   private final char[] mSelector = new char[CBConstants.MAX_SELECTORS];
   @NotNull
   private final char[] mSelectorMtf = new char[CBConstants.MAX_SELECTORS];
   @NotNull
   private final int[] mMtfFreq = new int[CBConstants.MAX_ALPHA_SIZE];
   @NotNull
   private final int[] mIncs = {1, 4, 13, 40, 121, 364, 1093, 3280, 9841, 29524, 88573, 265720,
         797161, 2391484};
   private final int mWorkFactor;
   private int mLast;
   private int mOrigPtr;
   private boolean mBlockRandomised;
   private int mBsBuff;
   private int mBsLive;
   private int mNInUse;
   private char[] mBlock;
   private int[] mQuadrant;
   private int[] mZptr;
   private short[] mSzptr;
   private int[] mFtab;
   private int mNMTF;
   private int mWorkDone;
   private int mWorkLimit;
   private boolean mFirstAttempt;
   private int mCurrentChar = -1;
   private int mRunLength;
   private boolean mClosed;
   private int mCombinedCRC;
   private int mAllowableBlockSize;
   private OutputStream mBsStream;

   public CBOutputStream(@NotNull Path pPath) throws IOException {
      this(Files.newOutputStream(pPath));
   }

   public CBOutputStream(@NotNull File pFile) throws IOException {
      this(new FileOutputStream(pFile));
   }

   public CBOutputStream(OutputStream pInStream) throws IOException {
      mBlock = null;
      mQuadrant = null;
      mZptr = null;
      mFtab = null;

      bsSetStream(pInStream);

      mWorkFactor = 50;
      mBlockSize100k = 9;
      allocateCompressStructures();
      initialize();
      initBlock();
   }

   private static void hbMakeCodeLengths(char[] pLen, int[] pFreq, int pAlphaSize) {
      int i;

      int[] heap = new int[CBConstants.MAX_ALPHA_SIZE + 2];
      int[] weight = new int[(CBConstants.MAX_ALPHA_SIZE << 1)];
      int[] parent = new int[(CBConstants.MAX_ALPHA_SIZE << 1)];

      for (i = 0; i < pAlphaSize; i++) {
         weight[i + 1] = ((pFreq[i] == 0) ? 1 : pFreq[i]) << 8;
      }

      while (true) {
         int nNodes = pAlphaSize;
         int nHeap = 0;

         heap[0] = 0;
         weight[0] = 0;
         parent[0] = -2;

         for (i = 1; i <= pAlphaSize; i++) {
            parent[i] = -1;
            nHeap++;
            heap[nHeap] = i;
            int zz, tmp;
            zz = nHeap;
            tmp = heap[zz];
            while (weight[tmp] < weight[heap[zz >> 1]]) {
               heap[zz] = heap[zz >> 1];
               zz >>= 1;
            }
            heap[zz] = tmp;
         }

         while (nHeap > 1) {
            int n1 = heap[1];
            heap[1] = heap[nHeap];
            nHeap--;
            {
               int zz, tmp;
               zz = 1;
               tmp = heap[zz];
               while (true) {
                  int yy = zz << 1;
                  if (yy > nHeap) {
                     break;
                  }
                  if ((yy < nHeap) && (weight[heap[yy + 1]] < weight[heap[yy]])) {
                     yy++;
                  }
                  if (weight[tmp] < weight[heap[yy]]) {
                     break;
                  }
                  heap[zz] = heap[yy];
                  zz = yy;
               }
               heap[zz] = tmp;
            }
            int n2 = heap[1];
            heap[1] = heap[nHeap];
            nHeap--;
            {
               int zz, tmp;
               zz = 1;
               tmp = heap[zz];
               while (true) {
                  int yy = zz << 1;
                  if (yy > nHeap) {
                     break;
                  }
                  if ((yy < nHeap) && (weight[heap[yy + 1]] < weight[heap[yy]])) {
                     yy++;
                  }
                  if (weight[tmp] < weight[heap[yy]]) {
                     break;
                  }
                  heap[zz] = heap[yy];
                  zz = yy;
               }
               heap[zz] = tmp;
            }
            nNodes++;
            parent[n1] = parent[n2] = nNodes;

            int i1 = ((weight[n1] & 0x000000ff) > (weight[n2] & 0x000000ff)) ? n1 : n2;
            weight[nNodes] = ((weight[n1] & 0xffffff00) + (weight[n2] & 0xffffff00)) |
                             (1 + (weight[i1] & 0x000000ff));

            parent[nNodes] = -1;
            nHeap++;
            heap[nHeap] = nNodes;
            int zz = nHeap, tmp = heap[zz];
            while (weight[tmp] < weight[heap[zz >> 1]]) {
               heap[zz] = heap[zz >> 1];
               zz >>= 1;
            }
            heap[zz] = tmp;
         }

         boolean shortLen = true;
         int j;
         for (i = 1; i <= pAlphaSize; i++) {
            j = 0;
            int k = i;
            while (parent[k] >= 0) {
               k = parent[k];
               j++;
            }
            pLen[i - 1] = (char) j;
            if (j > 20) {
               shortLen = false;
            }
         }

         if (shortLen) {
            break;
         }

         for (i = 1; i < pAlphaSize; i++) {
            j = weight[i] >> 8;
            j = 1 + (j / 2);
            weight[i] = j << 8;
         }
      }
   }

   private static void hbAssignCodes(int[] pCode, char[] pLength, int pMinLen, int pMaxLen,
         int pAlphaSize) {
      int vec;

      vec = 0;
      for (int n = pMinLen; n <= pMaxLen; n++) {
         for (int i = 0; i < pAlphaSize; i++) {
            if (pLength[i] == n) {
               pCode[i] = vec;
               vec++;
            }
         }
         vec <<= 1;
      }
   }

   @Contract(pure = true)
   private static char med3(char pA, char pB, char pC) {
      if (pA > pB) {
         char t = pA;
         pA = pB;
         pB = t;
      }
      if (pB > pC) {
         pB = pC;
      }
      if (pA > pB) {
         return pA;
      }
      return pB;
   }

   @Contract(pure = true)
   @Override
   public @NotNull String toString() {
      return "CBOutputStream{ }";
   }

   @SuppressWarnings("ProhibitedExceptionDeclared")
   @Override
   protected void finalize() throws Throwable {
      close();
      super.finalize();
   }

   @Contract(mutates = "this")
   private void bsSetStream(OutputStream pOutputStream) {
      mBsStream = pOutputStream;
      mBsLive = 0;
      mBsBuff = 0;
   }

   @Contract(mutates = "this")
   private void allocateCompressStructures() {
      int n = CBConstants.BASE_BLOCK_SIZE * mBlockSize100k;
      mBlock = new char[(n + 1 + CBConstants.NUM_OVERSHOOT_BYTES)];
      mQuadrant = new int[(n + CBConstants.NUM_OVERSHOOT_BYTES)];
      mZptr = new int[n];
      mFtab = new int[65537];
      mSzptr = new short[2 * n];
   }

   private void initialize() throws IOException {
      bsPutUChar('h');
      bsPutUChar('0' + mBlockSize100k);

      mCombinedCRC = 0;
   }

   private void initBlock() {
      // blockNo++;
      mCrc.initialiseCRC();
      mLast = -1;
      // ch = 0;

      for (int i = 0; i < 256; i++) {
         mInUse[i] = false;
      }

      /* 20 is just a paranoia constant */
      mAllowableBlockSize = (CBConstants.BASE_BLOCK_SIZE * mBlockSize100k) - 20;
   }

   private void bsPutUChar(int pC) throws IOException {
      bsW(8, pC);
   }

   private void bsW(int pN, int pV) throws IOException {
      while (mBsLive >= 8) {
         int i = mBsBuff >> 24;
         mBsStream.write(i);
         mBsBuff <<= 8;
         mBsLive -= 8;
      }
      mBsBuff |= pV << (32 - mBsLive - pN);
      mBsLive += pN;
   }

   public final void writeChars(@NotNull CharSequence pStr) throws IOException {
      int len = pStr.length();
      for (int i = 0; i < len; i++) {
         int v = pStr.charAt(i);
         write((v >>> 8) & 0xFF);
         write(v & 0xFF);
      }
   }

   public final void writeChar(char pChar) throws IOException {
      write((pChar >>> 8) & 0xFF);
      write(pChar & 0xFF);
   }

   public final void writeInt(int pV) throws IOException {
      write((pV >>> 24) & 0xFF);
      write((pV >>> 16) & 0xFF);
      write((pV >>> 8) & 0xFF);
      write(pV & 0xFF);
   }

   private void makeMaps() {
      mNInUse = 0;
      for (int i = 0; i < 256; i++) {
         if (mInUse[i]) {
            mUnseqToSeq[i] = (char) mNInUse;
            mNInUse++;
         }
      }
   }

   /**
    * modified by Oliver Merkel, 010128
    */
   @Override
   public void write(int pBv) throws IOException {
      int i = (256 + pBv) % 256;
      if (mCurrentChar == -1) {
         mCurrentChar = i;
         mRunLength++;
      } else {
         if (mCurrentChar == i) {
            mRunLength++;
            if (mRunLength > 254) {
               writeRun();
               mCurrentChar = -1;
               mRunLength = 0;
            }
         } else {
            writeRun();
            mRunLength = 1;
            mCurrentChar = i;
         }
      }
   }

   @Override
   public void flush() throws IOException {
      super.flush();
      mBsStream.flush();
   }

   @Override
   public void close() throws IOException {
      if (mClosed) {
         return;
      }

      if (mRunLength > 0) {
         writeRun();
      }
      mCurrentChar = -1;
      endBlock();
      endCompression();
      mClosed = true;
      super.close();
      mBsStream.close();
   }

   private void writeRun() throws IOException {
      if (mLast < mAllowableBlockSize) {
         mInUse[mCurrentChar] = true;
         for (int i = 0; i < mRunLength; i++) {
            mCrc.updateCRC((char) mCurrentChar);
         }
         switch (mRunLength) {
            case 1:
               mLast++;
               mBlock[mLast + 1] = (char) mCurrentChar;
               break;
            case 2:
               mLast++;
               mBlock[mLast + 1] = (char) mCurrentChar;
               mLast++;
               mBlock[mLast + 1] = (char) mCurrentChar;
               break;
            case 3:
               mLast++;
               mBlock[mLast + 1] = (char) mCurrentChar;
               mLast++;
               mBlock[mLast + 1] = (char) mCurrentChar;
               mLast++;
               mBlock[mLast + 1] = (char) mCurrentChar;
               break;
            default:
               mInUse[mRunLength - 4] = true;
               mLast++;
               mBlock[mLast + 1] = (char) mCurrentChar;
               mLast++;
               mBlock[mLast + 1] = (char) mCurrentChar;
               mLast++;
               mBlock[mLast + 1] = (char) mCurrentChar;
               mLast++;
               mBlock[mLast + 1] = (char) mCurrentChar;
               mLast++;
               mBlock[mLast + 1] = (char) (mRunLength - 4);
               break;
         }
      } else {
         endBlock();
         initBlock();
         writeRun();
      }
   }

   private void endBlock() throws IOException {
      int blockCRC = mCrc.getFinalCRC();
      mCombinedCRC = (mCombinedCRC << 1) | (mCombinedCRC >>> 31);
      mCombinedCRC ^= blockCRC;

      /* sort the block and establish posn of original string */
      doReversibleTransformation();

      /*
       * A 6-byte block header, the value chosen arbitrarily as 0x314159265359 :-). A 32 bit
       * value does not really
       * give a strong enough guarantee that the value will not appear by chance in the
       * compressed datastream.
       * Worst-case probability of this event, for a 900k block, is about 2.0e-3 for 32 bits, 1
       * .0e-5 for 40 bits and
       * 4.0e-8 for 48 bits. For a compressed file of size 100Gb -- about 100000 blocks -- only a
       *  48-bit marker will
       * do. NB: normal compression/ decompression do *not* rely on these statistical properties.
       *  They are only
       * important when trying to recover blocks from damaged files.
       */
      bsPutUChar(0x31);
      bsPutUChar(0x41);
      bsPutUChar(0x59);
      bsPutUChar(0x26);
      bsPutUChar(0x53);
      bsPutUChar(0x59);

      /* Now the block's CBMap, so it is in a known place. */
      bsPutint(blockCRC);

      /* Now a single bit indicating randomisation. */
      if (mBlockRandomised) {
         bsW(1, 1);
      } else {
         bsW(1, 0);
      }

      /* Finally, block's contents proper. */
      moveToFrontCodeAndSend();
   }

   private void endCompression() throws IOException {
      /*
       * Now another magic 48-bit number, 0x177245385090, to indicate the end of the last block.
       * (sqrt(pi), if you
       * want to know. I did want to use e, but it contains too much repetition -- 27 18 28 18 28
       *  46 -- for me to
       * feel
       * statistically comfortable. Call me paranoid.)
       */
      bsPutUChar(0x17);
      bsPutUChar(0x72);
      bsPutUChar(0x45);
      bsPutUChar(0x38);
      bsPutUChar(0x50);
      bsPutUChar(0x90);

      bsPutint(mCombinedCRC);

      bsFinishedWithStream();
   }

   private void bsFinishedWithStream() throws IOException {
      while (mBsLive > 0) {
         int i = mBsBuff >> 24;
         mBsStream.write(i);
         mBsBuff <<= 8;
         mBsLive -= 8;
      }
   }

   private void bsPutint(int pU) throws IOException {
      bsW(8, (pU >> 24) & 0xff);
      bsW(8, (pU >> 16) & 0xff);
      bsW(8, (pU >> 8) & 0xff);
      bsW(8, pU & 0xff);
   }

   private void sendMTFValues() throws IOException {
      char[][] len = new char[CBConstants.N_GROUPS][CBConstants.MAX_ALPHA_SIZE];

      int v, t, i, j, gs, ge;
      int nSelectors = 0, alphaSize, selCtr;
      int nGroups;

      alphaSize = mNInUse + 2;
      for (t = 0; t < CBConstants.N_GROUPS; t++) {
         for (v = 0; v < alphaSize; v++) {
            len[t][v] = GREATER_ICOST;
         }
      }

      if (mNMTF < 200) {
         nGroups = 2;
      } else if (mNMTF < 600) {
         nGroups = 3;
      } else if (mNMTF < 1200) {
         nGroups = 4;
      } else if (mNMTF < 2400) {
         nGroups = 5;
      } else {
         nGroups = 6;
      }


      int nPart, remF;

      nPart = nGroups;
      remF = mNMTF;
      gs = 0;
      while (nPart > 0) {
         int tFreq = remF / nPart;
         ge = gs - 1;
         int aFreq = 0;
         while ((aFreq < tFreq) && (ge < (alphaSize - 1))) {
            ge++;
            aFreq += mMtfFreq[ge];
         }

         if ((ge > gs) && (nPart != nGroups) && (nPart != 1) && (((nGroups - nPart) % 2) == 1)) {
            aFreq -= mMtfFreq[ge];
            ge--;
         }

         for (v = 0; v < alphaSize; v++) {
            if ((v >= gs) && (v <= ge)) {
               len[nPart - 1][v] = LESSER_ICOST;
            } else {
               len[nPart - 1][v] = GREATER_ICOST;
            }
         }

         nPart--;
         gs = ge + 1;
         remF -= aFreq;
      }

      int[][] rfreq = new int[CBConstants.N_GROUPS][CBConstants.MAX_ALPHA_SIZE];
      short[] cost = new short[CBConstants.N_GROUPS];
      /*
       * Iterate up to N_ITERS times to improve the tables.
       */
      for (int iter = 0; iter < CBConstants.N_ITERS; iter++) {
         for (t = 0; t < nGroups; t++) {
            for (v = 0; v < alphaSize; v++) {
               rfreq[t][v] = 0;
            }
         }

         nSelectors = 0;
         gs = 0;
         while (gs < mNMTF) {
            /* Set group start & end marks. */
            ge = (gs + CBConstants.G_SIZE) - 1;
            if (ge >= mNMTF) {
               ge = mNMTF - 1;
            }

            /*
             * Calculate the cost of this group as coded by each of the coding tables.
             */
            for (t = 0; t < nGroups; t++) {
               cost[t] = 0;
            }

            if (nGroups == 6) {
               short cost0, cost1, cost2, cost3, cost4, cost5;
               cost0 = cost1 = cost2 = cost3 = cost4 = cost5 = 0;
               for (i = gs; i <= ge; i++) {
                  short icv = mSzptr[i];
                  cost0 += len[0][icv];
                  cost1 += len[1][icv];
                  cost2 += len[2][icv];
                  cost3 += len[3][icv];
                  cost4 += len[4][icv];
                  cost5 += len[5][icv];
               }
               cost[0] = cost0;
               cost[1] = cost1;
               cost[2] = cost2;
               cost[3] = cost3;
               cost[4] = cost4;
               cost[5] = cost5;
            } else {
               for (i = gs; i <= ge; i++) {
                  short icv = mSzptr[i];
                  for (t = 0; t < nGroups; t++) {
                     cost[t] += len[t][icv];
                  }
               }
            }

            /*
             * Find the coding table which is best for this group, and record its identity in the
             *  selector table.
             */
            int bc = 999999999;
            int bt = -1;
            for (t = 0; t < nGroups; t++) {
               if (cost[t] < bc) {
                  bc = cost[t];
                  bt = t;
               }
            }

            mSelector[nSelectors] = (char) bt;
            nSelectors++;

            for (i = gs; i <= ge; i++) {
               rfreq[bt][mSzptr[i]]++;
            }

            gs = ge + 1;
         }

         for (t = 0; t < nGroups; t++) {
            hbMakeCodeLengths(len[t], rfreq[t], alphaSize);
         }
      }

      char[] pos = new char[CBConstants.N_GROUPS];
      for (i = 0; i < nGroups; i++) {
         pos[i] = (char) i;
      }
      for (i = 0; i < nSelectors; i++) {
         char ll_i = mSelector[i];
         j = 0;
         char tmp = pos[j];
         while (ll_i != tmp) {
            j++;
            char tmp2 = tmp;
            tmp = pos[j];
            pos[j] = tmp2;
         }
         pos[0] = tmp;
         mSelectorMtf[i] = (char) j;
      }

      int[][] code = new int[CBConstants.N_GROUPS][CBConstants.MAX_ALPHA_SIZE];

      /* Assign actual codes for the tables. */
      for (t = 0; t < nGroups; t++) {
         int minLen = 32;
         int maxLen = 0;
         for (i = 0; i < alphaSize; i++) {
            if (len[t][i] > maxLen) {
               maxLen = len[t][i];
            }
            if (len[t][i] < minLen) {
               minLen = len[t][i];
            }
         }
         hbAssignCodes(code[t], len[t], minLen, maxLen, alphaSize);
      }

      boolean[] inUse16 = new boolean[16];
      for (i = 0; i < 16; i++) {
         inUse16[i] = false;
         for (j = 0; j < 16; j++) {
            if (mInUse[(i << 4) + j]) {
               inUse16[i] = true;
            }
         }
      }

      for (i = 0; i < 16; i++) {
         if (inUse16[i]) {
            bsW(1, 1);
         } else {
            bsW(1, 0);
         }
      }

      for (i = 0; i < 16; i++) {
         if (inUse16[i]) {
            for (j = 0; j < 16; j++) {
               if (mInUse[(i << 4) + j]) {
                  bsW(1, 1);
               } else {
                  bsW(1, 0);
               }
            }
         }
      }

      bsW(3, nGroups);
      bsW(15, nSelectors);
      for (i = 0; i < nSelectors; i++) {
         for (j = 0; j < mSelectorMtf[i]; j++) {
            bsW(1, 1);
         }
         bsW(1, 0);
      }

      for (t = 0; t < nGroups; t++) {
         int curr = len[t][0];
         bsW(5, curr);
         for (i = 0; i < alphaSize; i++) {
            while (curr < len[t][i]) {
               bsW(2, 2);
               curr++;
            }
            while (curr > len[t][i]) {
               bsW(2, 3);
               curr--;
            }
            bsW(1, 0);
         }
      }

      selCtr = 0;
      gs = 0;
      while (gs < mNMTF) {
         ge = (gs + CBConstants.G_SIZE) - 1;
         if (ge >= mNMTF) {
            ge = mNMTF - 1;
         }
         for (i = gs; i <= ge; i++) {
            bsW(len[mSelector[selCtr]][mSzptr[i]], code[mSelector[selCtr]][mSzptr[i]]);
         }

         gs = ge + 1;
         selCtr++;
      }
   }

   private void moveToFrontCodeAndSend() throws IOException {
      bsW(24, mOrigPtr);
      generateMTFValues();
      sendMTFValues();
   }

   private void simpleSort(int pLo, int pHi, int pD) {
      int bigN, hp;

      bigN = (pHi - pLo) + 1;
      if (bigN < 2) {
         return;
      }

      hp = 0;
      while (mIncs[hp] < bigN) {
         hp++;
      }
      hp--;

      for (; hp >= 0; hp--) {
         int h = mIncs[hp];

         int i = pLo + h;
         while (i <= pHi) {
            /* copy 1 */
            int v = mZptr[i];
            int j = i;
            while (fullGtU(mZptr[j - h] + pD, v + pD)) {
               mZptr[j] = mZptr[j - h];
               j -= h;
               if (j <= ((pLo + h) - 1)) {
                  break;
               }
            }
            mZptr[j] = v;
            i++;

            /* copy 2 */
            if (i > pHi) {
               break;
            }
            v = mZptr[i];
            j = i;
            while (fullGtU(mZptr[j - h] + pD, v + pD)) {
               mZptr[j] = mZptr[j - h];
               j -= h;
               if (j <= ((pLo + h) - 1)) {
                  break;
               }
            }
            mZptr[j] = v;
            i++;

            /* copy 3 */
            if (i > pHi) {
               break;
            }
            v = mZptr[i];
            j = i;
            while (fullGtU(mZptr[j - h] + pD, v + pD)) {
               mZptr[j] = mZptr[j - h];
               j -= h;
               if (j <= ((pLo + h) - 1)) {
                  break;
               }
            }
            mZptr[j] = v;
            i++;

            if ((mWorkDone > mWorkLimit) && mFirstAttempt) {
               return;
            }
         }
      }
   }

   private void vswap(int p1, int p2, int pN) {
      while (pN > 0) {
         int temp = mZptr[p1];
         mZptr[p1] = mZptr[p2];
         mZptr[p2] = temp;
         p1++;
         p2++;
         pN--;
      }
   }

   private void qSort3(int pLoSt, int pHiSt) {
      int sp;
      @SuppressWarnings("LocalVariableOfConcreteClass") StackElem[] stack
            = new StackElem[QSORT_STACK_SIZE];
      for (int count = 0; count < QSORT_STACK_SIZE; count++) {
         stack[count] = new StackElem();
      }

      sp = 0;

      stack[sp].mLl = pLoSt;
      stack[sp].mHh = pHiSt;
      stack[sp].mDd = 2;
      sp++;

      while (sp > 0) {

         sp--;
         int lo = stack[sp].mLl;
         int hi = stack[sp].mHh;
         int i = stack[sp].mDd;

         if (((hi - lo) < SMALL_THRESH) || (i > DEPTH_THRESH)) {
            simpleSort(lo, hi, i);
            if ((mWorkDone > mWorkLimit) && mFirstAttempt) {
               return;
            }
            continue;
         }

         int med = med3(mBlock[mZptr[lo] + i + 1], mBlock[mZptr[hi] + i + 1],
                        mBlock[mZptr[(lo + hi) >> 1] + i + 1]);

         int ltLo;
         int unLo = ltLo = lo;
         int gtHi;
         int unHi = gtHi = hi;

         int n;
         while (true) {
            while (unLo <= unHi) {
               n = mBlock[mZptr[unLo] + i + 1] - med;
               if (n == 0) {
                  int temp;
                  temp = mZptr[unLo];
                  mZptr[unLo] = mZptr[ltLo];
                  mZptr[ltLo] = temp;
                  ltLo++;
                  unLo++;
                  continue;
               }
               if (n > 0) {
                  break;
               }
               unLo++;
            }
            while (unLo <= unHi) {
               n = mBlock[mZptr[unHi] + i + 1] - med;
               if (n == 0) {
                  int temp;
                  temp = mZptr[unHi];
                  mZptr[unHi] = mZptr[gtHi];
                  mZptr[gtHi] = temp;
                  gtHi--;
                  unHi--;
                  continue;
               }

               if (n < 0) {
                  break;
               }
               unHi--;
            }
            if (unLo > unHi) {
               break;
            }
            int temp;
            temp = mZptr[unLo];
            mZptr[unLo] = mZptr[unHi];
            mZptr[unHi] = temp;
            unLo++;
            unHi--;
         }

         if (gtHi < ltLo) {
            stack[sp].mLl = lo;
            stack[sp].mHh = hi;
            stack[sp].mDd = i + 1;
            sp++;
            continue;
         }

         n = Math.min(ltLo - lo, unLo - ltLo);
         vswap(lo, unLo - n, n);
         int m = Math.min(hi - gtHi, gtHi - unHi);
         vswap(unLo, (hi - m) + 1, m);

         n = (lo + unLo) - ltLo - 1;
         m = (hi - (gtHi - unHi)) + 1;

         stack[sp].mLl = lo;
         stack[sp].mHh = n;
         stack[sp].mDd = i;
         sp++;

         stack[sp].mLl = n + 1;
         stack[sp].mHh = m - 1;
         stack[sp].mDd = i + 1;
         sp++;

         stack[sp].mLl = m;
         stack[sp].mHh = hi;
         stack[sp].mDd = i;
         sp++;
      }
   }

   private void mainSort() {
      int i;

      for (i = 0; i < CBConstants.NUM_OVERSHOOT_BYTES; i++) {
         mBlock[mLast + i + 2] = mBlock[(i % (mLast + 1)) + 1];
      }
      for (i = 0; i <= (mLast + CBConstants.NUM_OVERSHOOT_BYTES); i++) {
         mQuadrant[i] = 0;
      }

      mBlock[0] = mBlock[mLast + 1];

      if (mLast < 4000) {
         for (i = 0; i <= mLast; i++) {
            mZptr[i] = i;
         }
         mFirstAttempt = false;
         mWorkDone = mWorkLimit = 0;
         simpleSort(0, mLast, 0);
      } else {
         boolean[] bigDone = new boolean[256];
         for (i = 0; i <= 255; i++) {
            bigDone[i] = false;
         }

         for (i = 0; i <= 65536; i++) {
            mFtab[i] = 0;
         }

         int c1 = mBlock[0];
         int c2;
         for (i = 0; i <= mLast; i++) {
            c2 = mBlock[i + 1];
            mFtab[(c1 << 8) + c2]++;
            c1 = c2;
         }

         for (i = 1; i <= 65536; i++) {
            mFtab[i] += mFtab[i - 1];
         }

         c1 = mBlock[1];
         int j;
         for (i = 0; i < mLast; i++) {
            c2 = mBlock[i + 2];
            j = (c1 << 8) + c2;
            c1 = c2;
            mFtab[j]--;
            mZptr[mFtab[j]] = i;
         }

         j = (mBlock[mLast + 1] << 8) + mBlock[1];
         mFtab[j]--;
         mZptr[mFtab[j]] = mLast;

         /*
          * Now ftab contains the first loc of every small bucket. Calculate the running order,
          * from smallest to
          * largest big bucket.
          */

         int[] runningOrder = new int[256];
         for (i = 0; i <= 255; i++) {
            runningOrder[i] = i;
         }

         int h = 1;
         do {
            h = (3 * h) + 1;
         } while (h <= 256);
         do {
            h /= 3;
            for (i = h; i <= 255; i++) {
               int vv = runningOrder[i];
               j = i;
               while ((mFtab[(runningOrder[j - h] + 1) << 8] - mFtab[runningOrder[j - h] << 8]) >
                      (mFtab[(vv + 1) << 8] - mFtab[vv << 8])) {
                  runningOrder[j] = runningOrder[j - h];
                  j -= h;
                  if (j <= (h - 1)) {
                     break;
                  }
               }
               runningOrder[j] = vv;
            }
         } while (h != 1);

         /*
          * The main sorting loop.
          */
         int[] copy = new int[256];
         for (i = 0; i <= 255; i++) {

            /*
             * Process big buckets, starting with the least full.
             */
            int ss = runningOrder[i];

            /*
             * Complete the big bucket [ss] by quicksorting any unsorted small buckets [ss, j].
             * Hopefully previous
             * pointer-scanning phases have already completed many of the small buckets [ss, j],
             * so we don't
             * have to
             * sort them at all.
             */
            for (j = 0; j <= 255; j++) {
               int sb = (ss << 8) + j;
               if ((mFtab[sb] & SETMASK) != SETMASK) {
                  int lo = mFtab[sb] & CLEARMASK;
                  int hi = (mFtab[sb + 1] & CLEARMASK) - 1;
                  if (hi > lo) {
                     qSort3(lo, hi);
                     if ((mWorkDone > mWorkLimit) && mFirstAttempt) {
                        return;
                     }
                  }
                  mFtab[sb] |= SETMASK;
               }
            }

            /*
             * The ss big bucket is now done. Record this fact, and update the quadrant
             * descriptors. Remember to
             * update quadrants in the overshoot area too, if necessary. The "if (i < 255)" test
             * merely skips this
             * updating for the last bucket processed, since updating for the last bucket is
             * pointless.
             */
            bigDone[ss] = true;

            if (i < 255) {
               int bbStart = mFtab[ss << 8] & CLEARMASK;
               int bbSize = (mFtab[(ss + 1) << 8] & CLEARMASK) - bbStart;
               int shifts = 0;

               while ((bbSize >> shifts) > 65534) {
                  shifts++;
               }

               for (j = 0; j < bbSize; j++) {
                  int a2update = mZptr[bbStart + j];
                  int qVal = j >> shifts;
                  mQuadrant[a2update] = qVal;
                  if (a2update < CBConstants.NUM_OVERSHOOT_BYTES) {
                     mQuadrant[a2update + mLast + 1] = qVal;
                  }
               }

            }

            /*
             * Now scan this big bucket so as to synthesise the sorted order for small buckets
             * [t, ss] for all t !=
             * ss.
             */
            for (j = 0; j <= 255; j++) {
               copy[j] = mFtab[(j << 8) + ss] & CLEARMASK;
            }

            for (j = mFtab[ss << 8] & CLEARMASK; j < (mFtab[(ss + 1) << 8] & CLEARMASK); j++) {
               c1 = mBlock[mZptr[j]];
               if (!bigDone[c1]) {
                  mZptr[copy[c1]] = (mZptr[j] == 0) ? mLast : (mZptr[j] - 1);
                  copy[c1]++;
               }
            }

            for (j = 0; j <= 255; j++) {
               mFtab[(j << 8) + ss] |= SETMASK;
            }
         }
      }
   }

   private void randomiseBlock() {
      int i;
      int rNToGo = 0;
      int rTPos = 0;
      for (i = 0; i < 256; i++) {
         mInUse[i] = false;
      }

      for (i = 0; i <= mLast; i++) {
         if (rNToGo == 0) {
            rNToGo = (char) CBConstants.R_NUMS[rTPos];
            rTPos++;
            if (rTPos == 512) {
               rTPos = 0;
            }
         }
         rNToGo--;
         mBlock[i + 1] ^= (rNToGo == 1) ? 1 : 0;
         // handle 16 bit signed numbers
         mBlock[i + 1] &= 0xFF;

         mInUse[mBlock[i + 1]] = true;
      }
   }

   private void doReversibleTransformation() {

      mWorkLimit = mWorkFactor * mLast;
      mWorkDone = 0;
      mBlockRandomised = false;
      mFirstAttempt = true;

      mainSort();

      if ((mWorkDone > mWorkLimit) && mFirstAttempt) {
         randomiseBlock();
         mWorkLimit = mWorkDone = 0;
         mBlockRandomised = true;
         mFirstAttempt = false;
         mainSort();
      }

      mOrigPtr = -1;
      for (int i = 0; i <= mLast; i++) {
         if (mZptr[i] == 0) {
            mOrigPtr = i;
            break;
         }
      }
   }

   @Contract(mutates = "this")
   private boolean fullGtU(int pI1, int pI2) {
      int k;
      char c1, c2;

      c1 = mBlock[pI1 + 1];
      c2 = mBlock[pI2 + 1];
      if (c1 != c2) {
         return c1 > c2;
      }
      pI1++;
      pI2++;

      c1 = mBlock[pI1 + 1];
      c2 = mBlock[pI2 + 1];
      if (c1 != c2) {
         return c1 > c2;
      }
      pI1++;
      pI2++;

      c1 = mBlock[pI1 + 1];
      c2 = mBlock[pI2 + 1];
      if (c1 != c2) {
         return c1 > c2;
      }
      pI1++;
      pI2++;

      c1 = mBlock[pI1 + 1];
      c2 = mBlock[pI2 + 1];
      if (c1 != c2) {
         return c1 > c2;
      }
      pI1++;
      pI2++;

      c1 = mBlock[pI1 + 1];
      c2 = mBlock[pI2 + 1];
      if (c1 != c2) {
         return c1 > c2;
      }
      pI1++;
      pI2++;

      c1 = mBlock[pI1 + 1];
      c2 = mBlock[pI2 + 1];
      if (c1 != c2) {
         return c1 > c2;
      }
      pI1++;
      pI2++;

      k = mLast + 1;

      do {
         c1 = mBlock[pI1 + 1];
         c2 = mBlock[pI2 + 1];
         if (c1 != c2) {
            return c1 > c2;
         }
         int s1 = mQuadrant[pI1];
         int s2 = mQuadrant[pI2];
         if (s1 != s2) {
            return s1 > s2;
         }
         pI1++;
         pI2++;

         c1 = mBlock[pI1 + 1];
         c2 = mBlock[pI2 + 1];
         if (c1 != c2) {
            return c1 > c2;
         }
         s1 = mQuadrant[pI1];
         s2 = mQuadrant[pI2];
         if (s1 != s2) {
            return s1 > s2;
         }
         pI1++;
         pI2++;

         c1 = mBlock[pI1 + 1];
         c2 = mBlock[pI2 + 1];
         if (c1 != c2) {
            return c1 > c2;
         }
         s1 = mQuadrant[pI1];
         s2 = mQuadrant[pI2];
         if (s1 != s2) {
            return s1 > s2;
         }
         pI1++;
         pI2++;

         c1 = mBlock[pI1 + 1];
         c2 = mBlock[pI2 + 1];
         if (c1 != c2) {
            return c1 > c2;
         }
         s1 = mQuadrant[pI1];
         s2 = mQuadrant[pI2];
         if (s1 != s2) {
            return s1 > s2;
         }
         pI1++;
         pI2++;

         if (pI1 > mLast) {
            pI1 -= mLast;
            pI1--;
         }
         if (pI2 > mLast) {
            pI2 -= mLast;
            pI2--;
         }

         k -= 4;
         mWorkDone++;
      } while (k >= 0);

      return false;
   }

   private void generateMTFValues() {
      char[] yy = new char[256];
      int i;
      int zPend;
      int wr;
      int EOB;

      makeMaps();
      EOB = mNInUse + 1;

      for (i = 0; i <= EOB; i++) {
         mMtfFreq[i] = 0;
      }

      wr = 0;
      zPend = 0;
      for (i = 0; i < mNInUse; i++) {
         yy[i] = (char) i;
      }

      for (i = 0; i <= mLast; i++) {
         char ll_i;

         ll_i = mUnseqToSeq[mBlock[mZptr[i]]];

         int j = 0;
         char tmp = yy[j];
         while (ll_i != tmp) {
            j++;
            char tmp2 = tmp;
            tmp = yy[j];
            yy[j] = tmp2;
         }
         yy[0] = tmp;

         if (j == 0) {
            zPend++;
         } else {
            if (zPend > 0) {
               zPend--;
               while (true) {
                  switch (zPend % 2) {
                     case 0:
                        mSzptr[wr] = CBConstants.RUNA;
                        wr++;
                        mMtfFreq[CBConstants.RUNA]++;
                        break;
                     case 1:
                        mSzptr[wr] = CBConstants.RUNB;
                        wr++;
                        mMtfFreq[CBConstants.RUNB]++;
                        break;
                  }
                  if (zPend < 2) {
                     break;
                  }
                  zPend = (zPend - 2) / 2;
               }
               zPend = 0;
            }
            mSzptr[wr] = (short) (j + 1);
            wr++;
            mMtfFreq[j + 1]++;
         }
      }

      if (zPend > 0) {
         zPend--;
         while (true) {
            switch (zPend % 2) {
               case 0:
                  mSzptr[wr] = CBConstants.RUNA;
                  wr++;
                  mMtfFreq[CBConstants.RUNA]++;
                  break;
               case 1:
                  mSzptr[wr] = CBConstants.RUNB;
                  wr++;
                  mMtfFreq[CBConstants.RUNB]++;
                  break;
            }
            if (zPend < 2) {
               break;
            }
            zPend = (zPend - 2) / 2;
         }
      }

      mSzptr[wr] = (short) EOB;
      wr++;
      mMtfFreq[EOB]++;

      mNMTF = wr;
   }

   private static class StackElem {

      int mLl;
      int mHh;
      int mDd;

      @Override
      public String toString() {
         return "StackElem{}";
      }
   }
}
