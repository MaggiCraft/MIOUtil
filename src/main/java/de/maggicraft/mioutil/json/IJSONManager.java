package de.maggicraft.mioutil.json;

import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;

/**
 * Common interface for {@link IReadableManager} and {@link IStorableManager}.
 * <br>
 * created on 2019.05.15 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface IJSONManager {

   /**
    * @return path in which the JSON-object is stored.
    */
   @NotNull Path getPath();

}
