package de.maggicraft.mioutil.json;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

/**
 * Functional module for reading properties of a given JSONObject.
 * <p>
 * created on 2019.08.29 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
@SuppressWarnings({"unused", "ChainOfInstanceofChecks"})
public final class ReadableUtil {

   private ReadableUtil() {}

   public static boolean contains(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      return pJSON.containsKey(pUID.getUID());
   }

   public static boolean getBool(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      return (Boolean) getObject(pJSON, pUID);
   }

   public static byte getByte(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      Object object = getObject(pJSON, pUID);
      if (object instanceof Long) {
         long number = (long) (Long) object;
         insideBounds(Byte.MIN_VALUE, Byte.MAX_VALUE, number, pUID);
         return (byte) number;
      } else if (object instanceof Integer) {
         int number = (int) (Integer) object;
         insideBounds(Byte.MIN_VALUE, Byte.MAX_VALUE, number, pUID);
         return (byte) number;
      } else {
         throw new NonPrimitiveTypeException(object, "byte");
      }
   }

   public static short getShort(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      Object object = getObject(pJSON, pUID);
      if (object instanceof Long) {
         long number = (long) (Long) object;
         insideBounds(Short.MIN_VALUE, Short.MAX_VALUE, number, pUID);
         return (short) number;
      } else if (object instanceof Integer) {
         int number = (int) (Integer) object;
         insideBounds(Short.MIN_VALUE, Short.MAX_VALUE, number, pUID);
         return (short) number;
      } else {
         throw new NonPrimitiveTypeException(object, "short");
      }
   }

   public static char getChar(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      Object object = getObject(pJSON, pUID);
      if (object instanceof Long) {
         long number = (long) (Long) object;
         insideBounds(Character.MIN_VALUE, Character.MAX_VALUE, number, pUID);
         return (char) number;
      } else if (object instanceof Integer) {
         int number = (int) (Integer) object;
         insideBounds(Character.MIN_VALUE, Character.MAX_VALUE, number, pUID);
         return (char) number;
      } else {
         throw new NonPrimitiveTypeException(object, "char");
      }
   }

   public static int getInt(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      Object object = getObject(pJSON, pUID);
      if (object instanceof Long) {
         long number = (long) (Long) object;
         insideBounds(Integer.MIN_VALUE, Integer.MAX_VALUE, number, pUID);
         return (int) number;
      } else if (object instanceof Integer) {
         return (int) (Integer) object;
      } else {
         throw new NonPrimitiveTypeException(object, "int");
      }
   }

   public static long getLong(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      return (long) (Long) getObject(pJSON, pUID);
   }

   public static float getFloat(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      Object object = getObject(pJSON, pUID);
      if (object instanceof Double) {
         double number = (double) (Double) object;
         insideBounds(Float.MIN_VALUE, Float.MAX_VALUE, number, pUID);
         return (float) number;
      } else if (object instanceof Long) {
         long number = (long) (Long) object;
         insideBounds(Float.MIN_VALUE, Float.MAX_VALUE, number, pUID);
         return number;
      } else if (object instanceof Integer) {
         int number = (int) (Integer) object;
         insideBounds(Float.MIN_VALUE, Float.MAX_VALUE, number, pUID);
         return number;
      } else {
         throw new NonPrimitiveTypeException(object, "float");
      }
   }

   public static double getDouble(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      Object object = getObject(pJSON, pUID);
      if (object instanceof Double) {
         return (double) (Double) object;
      } else if (object instanceof Long) {
         long number = (long) (Long) object;
         insideBounds(Double.MIN_VALUE, Double.MAX_VALUE, number, pUID);
         return number;
      } else if (object instanceof Integer) {
         int number = (int) (Integer) object;
         insideBounds(Double.MIN_VALUE, Double.MAX_VALUE, number, pUID);
         return number;
      } else {
         throw new NonPrimitiveTypeException(object, "double");
      }
   }

   public static Integer getIntObject(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      Object object = getObject(pJSON, pUID);
      if (object instanceof Long) {
         long number = (long) (Long) object;
         insideBounds(Integer.MIN_VALUE, Integer.MAX_VALUE, number, pUID);
         return (Integer) (int) number;
      } else if (object instanceof Integer) {
         return (Integer) object;
      } else {
         throw new NonPrimitiveTypeException(object, "integer");
      }
   }

   @NotNull
   public static Long getLongObject(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      Object object = getObject(pJSON, pUID);
      if (object instanceof Long) {
         return (Long) object;
      } else if (object instanceof Integer) {
         return (Long) (long) (Integer) object;
      } else {
         throw new NonPrimitiveTypeException(object, "long");
      }
   }

   @NotNull
   public static Double getDoubleObject(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      Object object = getObject(pJSON, pUID);
      if (object instanceof Double) {
         return (Double) object;
      } else if (object instanceof Long) {
         long number = (long) (Long) object;
         insideBounds(Double.MIN_VALUE, Double.MAX_VALUE, number, pUID);
         return (double) number;
      } else if (object instanceof Integer) {
         int number = (int) (Integer) object;
         insideBounds(Double.MIN_VALUE, Double.MAX_VALUE, number, pUID);
         return (double) number;
      } else {
         throw new NonPrimitiveTypeException(object, "double");
      }
   }

   private static void insideBounds(long pMin, long pMax, long pValue,
         @NotNull IUniqueID<String> pUID) {
      if (pValue < pMin) {
         String description = "%s: the value %s is smaller than the specified bound %s";
         throw new IllegalArgumentException(String.format(description, pUID, pValue, pMin));
      } else if (pValue > pMax) {
         String description = "%s: the value %s is greater than the specified bound %s";
         throw new IllegalArgumentException(String.format(description, pUID, pValue, pMax));
      }
   }

   private static void insideBounds(double pMin, double pMax, double pValue,
         @NotNull IUniqueID<String> pUID) {
      if (pValue < pMin) {
         String description = "%s: the value %s is smaller than the specified bound %s";
         throw new IllegalArgumentException(String.format(description, pUID, pValue, pMin));
      } else if (pValue > pMax) {
         String description = "%s: the value %s is greater than the specified bound %s";
         throw new IllegalArgumentException(String.format(description, pUID, pValue, pMax));
      }
   }

   public static String[] getStringArray(@NotNull Map<?, ?> pJSON,
         @NotNull IUniqueID<String> pUID) {
      JSONArray array = getArray(pJSON, pUID);

      String[] texts = new String[array.size()];
      int i = 0;
      for (Object object : array) {
         texts[i] = (String) object;
         i++;
      }

      return texts;
   }

   @NotNull
   public static JSONArray getArray(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      return (JSONArray) getObject(pJSON, pUID);
   }

   @Contract("_, _ -> new")
   @Deprecated
   @NotNull
   public static LocalDateTime getLocalDateTime(@NotNull Map<?, ?> pJSON,
         @NotNull IUniqueID<String> pUID) {
      return LocalDateTime.ofInstant(Instant.ofEpochMilli(getLong(pJSON, pUID)),
                                     TimeZone.getDefault().toZoneId());
   }

   @Contract("_, _ -> new")
   @SuppressWarnings("UseOfObsoleteDateTimeApi")
   @Deprecated
   @NotNull
   public static Date getDate(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      return new Date(getLong(pJSON, pUID));
   }

   @NotNull
   public static String getString(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      return (String) getObject(pJSON, pUID);
   }

   @NotNull
   public static JSONObject getJSON(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      return (JSONObject) getObject(pJSON, pUID);
   }

   @NotNull
   public static Object getObject(@NotNull Map<?, ?> pJSON, @NotNull IUniqueID<String> pUID) {
      return pJSON.get(pUID.getUID());
   }

}
