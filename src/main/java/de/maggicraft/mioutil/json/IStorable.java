package de.maggicraft.mioutil.json;

import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONObject;

/**
 * Implemented by abstract data models which can be stored.
 * <p>
 * created on 2019.05.11 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 * @see IStorableManager used by
 */
@FunctionalInterface
public interface IStorable {

   @NotNull JSONObject toJSON();

}
