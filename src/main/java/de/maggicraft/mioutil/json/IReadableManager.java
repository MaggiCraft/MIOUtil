package de.maggicraft.mioutil.json;

import de.maggicraft.mlog.MLog;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.nio.file.Files;

/**
 * created on 2019.05.15 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
public interface IReadableManager extends IJSONManager {

   default void read() {
      try {
         JSONObject json = (JSONObject) new JSONParser().parse(Files.newBufferedReader(getPath()));

         readHeader(json);

         readEntities((JSONArray) json.get("entities"));
      } catch (IOException | ParseException pE) {
         errorHandling(pE);
      }
   }

   default void errorHandling(Exception pE) {
      MLog.log(pE);
   }

   void readHeader(@NotNull JSONObject pJSON);

   void readEntities(@NotNull JSONArray pEntities);

}
