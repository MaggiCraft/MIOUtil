package de.maggicraft.mioutil.json;

import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.chrono.ChronoLocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * Functional module to add properties to a given JSON.
 * <p>
 * created on 2019.08.29 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
@SuppressWarnings({"unused", "rawtypes", "unchecked"})
public final class StorableUtil {

   private StorableUtil() { }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID,
         @NotNull Iterable<? extends IStorable> pStorables) {
      JSONArray lists = new JSONArray();
      for (IStorable storable : pStorables) {
         lists.add(storable.toJSON());
      }
      pJSON.put(pUID.getUID(), lists);
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID,
         @NotNull String[] pText) {
      JSONArray lists = new JSONArray();
      lists.addAll(Arrays.asList(pText));
      pJSON.put(pUID.getUID(), lists);
   }

   @Deprecated
   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID,
         @NotNull ChronoLocalDateTime<LocalDate> pDateTime) {
      long millis = pDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
      pJSON.put(pUID.getUID(), millis);
   }

   @SuppressWarnings("UseOfObsoleteDateTimeApi")
   @Deprecated
   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID,
         @NotNull Date pDate) {
      pJSON.put(pUID.getUID(), pDate.getTime());
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID,
         @NotNull String pText) {
      pJSON.put(pUID.getUID(), pText);
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID, int pInt) {
      pJSON.put(pUID.getUID(), pInt);
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID, long pLong) {
      pJSON.put(pUID.getUID(), pLong);
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID, float pFloat) {
      pJSON.put(pUID.getUID(), pFloat);
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID, double pDouble) {
      pJSON.put(pUID.getUID(), pDouble);
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID,
         @NotNull Integer pInteger) {
      pJSON.put(pUID.getUID(), pInteger);
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID,
         @NotNull Long pLong) {
      pJSON.put(pUID.getUID(), pLong);
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID,
         @NotNull Float pFloat) {
      pJSON.put(pUID.getUID(), pFloat);
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID,
         @NotNull Double pDouble) {
      pJSON.put(pUID.getUID(), pDouble);
   }

   public static void put(@NotNull Map pJSON, @NotNull IUniqueID<String> pUID,
         @NotNull JSONObject pJSONObject) {
      pJSON.put(pUID.getUID(), pJSONObject);
   }
}
