package de.maggicraft.mioutil.json;

/**
 * created on 2019.05.13 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
@FunctionalInterface
public interface IUID extends IUniqueID<String> {}
