package de.maggicraft.mioutil.json;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * Unique identifier of type E.
 * <br>
 * created on 2019.05.11 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
@FunctionalInterface
public interface IUniqueID<E> extends Serializable {

   @NotNull E getUID();

}
