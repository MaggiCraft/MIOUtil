package de.maggicraft.mioutil.json;


import de.maggicraft.mlog.MLog;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;

/**
 * created on 2019.05.11 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
public interface IStorableManager<V extends IStorable> extends IStorable, IJSONManager {

   default void store() {
      try (BufferedWriter writer = Files.newBufferedWriter(getPath())) {
         writer.write(toJSON().toJSONString());
         writer.flush();
      } catch (IOException pE) {
         MLog.log(pE);
      }
   }

   @SuppressWarnings("unchecked")
   @NotNull
   @Override
   default JSONObject toJSON() {
      JSONObject json = new JSONObject();

      storeHeader(json);

      Collection<V> storables = getStorables();
      JSONArray array = new JSONArray();
      for (V storable : storables) {
         array.add(storable.toJSON());
      }
      json.put("entities", array);

      return json;
   }

   void storeHeader(@NotNull JSONObject pJSON);

   @NotNull Collection<V> getStorables();

}
