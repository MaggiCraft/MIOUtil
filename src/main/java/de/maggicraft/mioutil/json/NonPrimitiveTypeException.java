package de.maggicraft.mioutil.json;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * created on 2020.10.29 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
public final class NonPrimitiveTypeException extends ClassCastException {

   public NonPrimitiveTypeException(@NotNull Object pObject, @NotNull String pType) {
      super(constructMessage(pObject, pType));
   }

   @NotNull
   @Contract(pure = true)
   private static String constructMessage(@NotNull Object pObject, @NotNull String pType) {
      String description = "could not cast %s to %s since the object is of type %s";
      return String.format(description, pObject, pType, pObject.getClass());
   }
}
