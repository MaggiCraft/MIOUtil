package de.maggicraft.mioutil.compr;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * created on 2019.03.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
@Deprecated
@SuppressWarnings("ALL")
public final class CompressedUtil {

   public static final int BITS = 32;

   @Contract(pure = true)
   private CompressedUtil() {}

   /**
    * Gibt zurück, wie viele Bits zum Speichern einer Zahl verwendet werden.
    *
    * @param pInts Menge von Zahlen die die größte Zahl enthält, die gespeichert wird
    * @return Anzahl an Bits zum Speichern einer Zahl
    */
   public static int bitLength(@NotNull int[] pInts) {
      int max = 0;
      for (int i : pInts) {
         if (i > max) {
            max = i;
         }
      }

      return bitLength(max);
   }

   public static int bitLength(@NotNull Iterable<Integer> pInts) {
      int max = 0;
      for (int i : pInts) {
         if (i > max) {
            max = i;
         }
      }

      return bitLength(max);
   }

   /**
    * Gibt die Anzahl der Bits zurück, die zum Speichern der Zahl benötigt werden
    *
    * @param pInt Zahl
    * @return Anzahl der Bits
    */
   public static int bitLength(int pInt) {
      return Integer.toBinaryString(pInt).length();
   }

}
