package de.maggicraft.mioutil.compr;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;

/**
 * Speichert eine Menge von Zahlen komprimiert.
 */
@Deprecated
@SuppressWarnings("ALL")
public class Compressed {

   @NotNull
   private final int[] mStorage;
   private final int mBits;
   private final int mUnits;

   /**
    * Erstellt eine Menge von komprimierten Zahlen.
    *
    * @param pInts Zahlen die komprimiert werden
    */
   public Compressed(@NotNull int... pInts) {
      this(pInts.length, CompressedUtil.bitLength(pInts));

      set(pInts);
   }

   /**
    * Erstellt die Speicherstruktur für eine Menge von komprimierten Zahlen.
    *
    * @param pUnits Anzahl von Zahlen die komprimiert gespeichert werden
    * @param pBits  Anzahl von Bits zum Speichern einer Zahl aufgewendet werden
    */
   public Compressed(int pUnits, int pBits) {
      mUnits = pUnits;
      mBits = pBits;

      int size = (int) Math.ceil((mUnits * mBits) / (float) CompressedUtil.BITS);
      mStorage = new int[size];
   }

   /**
    * Speichert die übergebenen Zahlen komprimiert.
    *
    * @param pInts Zahlen die komprimiert werden
    */
   public void set(@NotNull int... pInts) {
      for (int i = 0; i < pInts.length; i++) {
         set(i, pInts[i]);
      }
   }

   /**
    * Speichert die übergebene Zahl an der übergebenen Stelle.
    *
    * @param pUnit  Position an der die Zahl gespeichert wird
    * @param pValue Zahl die komprimiert gespeichert wird
    */
   public void set(int pUnit, int pValue) {
      for (int i = mBits - 1; i >= 0; i--) {
         int bit = (pUnit * mBits) + i;

         if (((pValue >> i) & 1) == 1) {
            mStorage[bit / CompressedUtil.BITS] |= 1 << (bit % CompressedUtil.BITS);
         } else {
            mStorage[bit / CompressedUtil.BITS] &= ~(1 << (bit % CompressedUtil.BITS));
         }
      }
   }

   /**
    * Dekomprimiert die Zahl an der übergebenen Stelle
    *
    * @param pUnit Stelle an der die zu dekomprimierende Zahl liegt
    * @return dekomprimierte Zahl
    */
   public int get(int pUnit) {
      int value = 0;
      for (int i = 0; i < mBits; i++) {
         int bit = (pUnit * mBits) + i;
         value |= ((mStorage[bit / CompressedUtil.BITS] >> (bit % CompressedUtil.BITS)) & 1) << i;
      }
      return value;
   }

   @Override
   public int hashCode() {
      int result = Objects.hash(mBits, mUnits);
      result = (31 * result) + Arrays.hashCode(mStorage);
      return result;
   }

   @Override
   public boolean equals(Object pObject) {
      if (this == pObject) {
         return true;
      }
      if ((pObject == null) || (getClass() != pObject.getClass())) {
         return false;
      }
      Compressed that = (Compressed) pObject;
      return (mBits == that.mBits) && (mUnits == that.mUnits) &&
             Arrays.equals(mStorage, that.mStorage);
   }

   public int size() {
      return mUnits;
   }

   public int getBits() {
      return mBits;
   }
}
