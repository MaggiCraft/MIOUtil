package de.maggicraft.mioutil.compr;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.HashSet;

/**
 * Speichert mehrere Strings in einem komprimierten Format.
 * <p>
 * Für die Dekomprimierung müssen alle verwendeten Chars bekannt sein, damit die {@link
 * CompressedText#mDecompress}-Table aufgestellt werden kann. Diese verknüpft die verwendeten Chars
 * mit einer Folge von natürlichen Zahlen. Die Decompress-Table kann mittels {@link
 * CompressedText#decompressTable(String...)} erzeugt werden. Die Tabelle für die Komprimierung wird
 * aus der Decompress-Table erstellt.
 * <p>
 * Die Kompressionsrate ist maßgeblich abhängig von der Anzahl der verschiedenen Buchstaben die
 * komprimiert werden. Bei unter 128 Chars liegt die Kompressionsrate bei ~2,1
 */
@Deprecated
@SuppressWarnings("ALL")
public class CompressedText {

   @NotNull
   private final int[][] mStorage;
   @NotNull
   private final char[] mDecompress;
   private final int mBits;
   @Nullable
   private int[] mCompress;

   /**
    * Erstellt die Speicherstruktur für eine Menge von komprimierten Text.
    *
    * @param pDecompress Decompress-Table
    * @param pQuantity   Anzahl der Strings die komprimiert gespeichert werden sollen
    */
   public CompressedText(@NotNull char[] pDecompress, int pQuantity) {
      mDecompress = pDecompress;
      mStorage = new int[pQuantity][];

      mCompress = compressTable(pDecompress);
      mBits = bitLength(pDecompress);
   }

   /**
    * Erstellt eine Menge von komprimierten Strings
    *
    * @param pTexts Text der komprimiert werden soll
    */
   public CompressedText(@NotNull String... pTexts) {
      mDecompress = decompressTable(pTexts);
      mStorage = new int[pTexts.length][];

      mCompress = compressTable(mDecompress);
      mBits = bitLength(mDecompress);

      set(pTexts);
   }

   /**
    * Erstellt die Compress-Table
    *
    * @param pDecompress Decompress-Table aus der die Kompress-Table erzeugt wird
    * @return Compress-Table
    */
   @NotNull
   public static int[] compressTable(@NotNull char[] pDecompress) {
      char max = Character.MIN_VALUE;
      for (char decompress : pDecompress) {
         if (decompress > max) {
            max = decompress;
         }
      }

      int[] table = new int[max + 1];
      for (char ch1 = 0; ch1 < table.length; ch1++) {
         for (char ch2 = 0; ch2 < pDecompress.length; ch2++) {
            if (pDecompress[ch2] == ch1) {
               table[ch1] = ch2;
            }
         }
      }

      return table;
   }

   /**
    * Gibt zurück, wie viele Bits zum Speichern eines Buchstaben verwendet werden.
    *
    * @param pDecompress Decompress-Table
    * @return Anzahl an Bits zum Speichern eines Buchstaben
    */
   public static int bitLength(@NotNull char[] pDecompress) {
      return CompressedUtil.bitLength(pDecompress.length);
   }

   /**
    * Erstellt die Decompress-Table für eine Menge von Buchstaben.
    *
    * @param pText Text der alle Buchstaben enthält, die komprimiert werden sollen.
    * @return Decompress-Table
    */
   @NotNull
   public static char[] decompressTable(@NotNull String... pText) {
      Collection<Character> difChars = new HashSet<>(128);
      for (String s : pText) {
         if (s != null) {
            for (int i = 0; i < s.length(); i++) {
               difChars.add(s.charAt(i));
            }
         }
      }

      char[] chars = new char[difChars.size() + 1];
      int i = 1;
      for (Character ch : difChars) {
         chars[i] = ch;
         i++;
      }

      return chars;
   }

   /**
    * Gibt zurück, wie viele Bits zum Speichern eines Buchstaben verwendet werden.
    *
    * @param pText Text der alle Buchstaben enthält die gespeichert werden
    * @return Anzahl an Bits zum Speichern eines Buchstaben
    */
   public static int bitLength(@NotNull String... pText) {
      Collection<Character> difChars = new HashSet<>(128);
      for (String s : pText) {
         if (s != null) {
            for (int i = 0; i < s.length(); i++) {
               difChars.add(s.charAt(i));
            }
         }
      }

      return CompressedUtil.bitLength(difChars.size() + 1);
   }

   /**
    * Speichert die übergebenen Strings komprimiert.
    *
    * @param pTexts Text der komprimiert gespeichert wird
    */
   public void set(@NotNull String... pTexts) {
      for (int i = 0; i < pTexts.length; i++) {
         if (pTexts[i] != null) {
            set(i, pTexts[i]);
         }
      }
      resetTable();
   }

   /**
    * Speichert den übergeben Text an übergebener Stelle
    *
    * @param pUnit Position an der der Text gespeichert wird
    * @param pText Text der komprimiert gespeichert wird
    */
   public void set(int pUnit, @NotNull CharSequence pText) {
      mStorage[pUnit] = new int[(int) Math
            .ceil((pText.length() * mBits) / (float) CompressedUtil.BITS)];

      for (int i = 0; i < pText.length(); i++) {
         int value = mCompress[pText.charAt(i)];
         for (int j = mBits - 1; j >= 0; j--) {
            setBit(pUnit, (i * mBits) + j, ((value >> j) & 1) == 1);
         }
      }
   }

   /**
    * Setzt die Compress-Table zurück. Dies kann notwendig sein, da sie viel Speicher verbraucht.
    */
   public void resetTable() {
      mCompress = null;
   }

   /**
    * Setzt ein einzelnes Bit an übergebener Stelle
    *
    * @param pUnit  Position des Texts
    * @param pBit   Position des Bits
    * @param pValue Wert des Bits
    */
   private void setBit(int pUnit, int pBit, boolean pValue) {
      if (pValue) {
         mStorage[pUnit][pBit / CompressedUtil.BITS] |= 1 << (pBit % CompressedUtil.BITS);
      } else {
         mStorage[pUnit][pBit / CompressedUtil.BITS] &= ~(1 << (pBit % CompressedUtil.BITS));
      }
   }

   /**
    * Dekomprimiert den Text der übergebenen Stelle
    *
    * @param pUnit Stelle an der der zu dekomprimierende Text liegt
    * @return dekomprimierter Text
    */
   @NotNull
   public String get(int pUnit) {
      String text = "";

      int maxChars = (mStorage[pUnit].length * CompressedUtil.BITS) / mBits;
      for (int i = 0; i < maxChars; i++) {
         int val = get(pUnit, i);
         if (val == 0) {
            return text;
         }
         text += mDecompress[val];
      }

      return text;
   }

   /**
    * Gibt ein Bit an übergebener Stelle zurück.
    *
    * @param pUnit Stelle des Texts
    * @param pBit  Stelle des Bits
    * @return Bit der gegebenen Stelle
    */
   public int get(int pUnit, int pBit) {
      int value = 0;
      for (int i = 0; i < mBits; i++) {
         int bit = (pBit * mBits) + i;
         value |=
               ((mStorage[pUnit][bit / CompressedUtil.BITS] >> (bit % CompressedUtil.BITS)) & 1) <<
               i;
      }
      return value;
   }

   public int[][] getStorage() {
      return mStorage;
   }

   public int size() {
      return mStorage.length;
   }

   public int decompressionLength() {
      return mDecompress.length;
   }

   public int getBits() {
      return mBits;
   }
}
