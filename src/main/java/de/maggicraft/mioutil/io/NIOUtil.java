package de.maggicraft.mioutil.io;

import de.maggicraft.mlog.MLog;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * created on 2020.10.16 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/MaggiCraft
 *
 * @author Marc Schmidt
 */
public final class NIOUtil {

   private NIOUtil() {}

   public static boolean write(@NotNull Path pPath, @NotNull String pText) {
      try (BufferedWriter writer = Files.newBufferedWriter(pPath)) {
         writer.write(pText);
         writer.flush();
         writer.close();
         return true;
      } catch (IOException pE) {
         MLog.log(pE);
         return false;
      }
   }

   public static void deleteFileOrFolder(@NotNull Path path) throws IOException {
      Files.walkFileTree(path, NIOUtilHelper.FILE_VISITOR);
   }

   private static class NIOUtilHelper {

      private static final FileVisitor<Path> FILE_VISITOR = new SimpleFileVisitor<Path>() {

         @Override
         public FileVisitResult visitFile(Path pFile, BasicFileAttributes pAttributes)
               throws IOException {
            Files.delete(pFile);
            return FileVisitResult.CONTINUE;
         }

         @Override
         public FileVisitResult visitFileFailed(Path pFile, IOException pE) {
            return handleException(pE);
         }

         @Override
         public FileVisitResult postVisitDirectory(Path pDir, IOException pE) throws IOException {
            if (pE == null) {
               Files.delete(pDir);
               return FileVisitResult.CONTINUE;
            } else {
               return handleException(pE);
            }
         }

         @SuppressWarnings("SameReturnValue")
         @NotNull
         private FileVisitResult handleException(IOException pE) {
            MLog.log(pE);
            return FileVisitResult.TERMINATE;
         }
      };
   }

}
