package de.maggicraft.mioutil.io;

import de.maggicraft.mioutil.cbstream.CBInputStream;
import de.maggicraft.mioutil.cbstream.CBOutputStream;
import de.maggicraft.mlog.MLog;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Eine Sammlung von Methoden zum Schreiben und Lesen von Texten und Listen in und aus wahlweisen
 * komprimierten Dateien.
 * <p>
 * Created by Marc Schmidt on 19.03.2017.
 */
@SuppressWarnings({"ALL"})
@Deprecated
public final class MIOUtil {

   public static final String LINE_SEP = System.getProperty("line.separator");
   private static final int INITIAL_CAPACITY = 512;

   private MIOUtil() {}

   /**
    * Schreibt die übergebene {@link List} in die übergebene Datei.
    * <p>
    * Dabei wird jedes MElement der {@link List} in eine neue Zeile geschrieben.
    *
    * @param pFile Datei in die geschrieben wird
    * @param pList Liste dessen Inhalt geschrieben wird
    */
   @Deprecated
   public static void write(@NotNull File pFile, @NotNull Iterable<?> pList) {
      write(pFile, pList, new StringBuilder(INITIAL_CAPACITY), false);
   }

   /**
    * Schreibt die übergebene {@link List}, sowie den Inhalt des {@link StringBuilder}s, wahlweise
    * komprimiert, in die übergebene Datei.
    *
    * @param pFile     Datei in die geschrieben wird
    * @param pList     Liste dessen Inhalt geschrieben wird
    * @param pSb       {@link StringBuilder} der bereits Content enthalten kann
    * @param pCompress {@code true:} die Datei wird komprimiert geschrieben, sonst {@code false}.
    */
   @Deprecated
   public static void write(@NotNull File pFile, Iterable<?> pList, @NotNull StringBuilder pSb,
         boolean pCompress) {
      for (Object obj : pList) {
         pSb.append(obj).append(LINE_SEP);
      }
      write(pFile, pSb.toString(), pCompress);
   }

   /**
    * Schreibt den übergebenen Text, wahlweise komprimiert, in die übergebene Datei.
    *
    * @param pFile     Datei in die geschrieben wird
    * @param pText     Text der geschrieben wird
    * @param pCompress {@code true:} die Datei wird komprimiert geschrieben, sonst {@code false}.
    */
   @Deprecated
   public static void write(@NotNull File pFile, @NotNull String pText, boolean pCompress) {
      if (pCompress) {
         writeCompressed(pFile, pText);
      } else {
         write(pFile, pText);
      }
   }

   /**
    * Schreibt den übergebenen Text komprimiert in die übergebene Datei.
    *
    * @param pFile Datei in die geschrieben wird
    * @param pText Text der geschrieben wird
    */
   @Deprecated
   public static void writeCompressed(@NotNull File pFile, @NotNull CharSequence pText) {
      try (CBOutputStream out = new CBOutputStream(pFile)) {
         out.writeChars(pText);
         out.flush();
      } catch (IOException pE) {
         MLog.log(pE);
      }
   }

   /**
    * Schreibt den übergebenen Text in die übergebene Datei.
    *
    * @param pFile Datei in die geschrieben wird
    * @param pText Text der geschrieben wird
    */
   @Deprecated
   public static void write(@NotNull File pFile, @NotNull String pText) {
      try (Writer fw = new OutputStreamWriter(new FileOutputStream(pFile),
                                              StandardCharsets.UTF_8)) {
         fw.write(pText);
         fw.flush();
      } catch (IOException pE) {
         MLog.log(pE);
      }
   }

   /**
    * Schreibt die übergebene {@link List}, wahlweise komprimiert, in die übergebene Datei.
    * <p>
    * Dabei wird jedes MElement der {@link List} in eine neue Zeile geschrieben.
    *
    * @param pFile     Datei in die geschrieben wird
    * @param pList     Liste dessen Inhalt geschrieben wird
    * @param pCompress {@code true:} die Datei wird komprimiert geschrieben, sonst {@code false}.
    */
   @Deprecated
   public static void write(@NotNull File pFile, @NotNull Iterable<?> pList, boolean pCompress) {
      write(pFile, pList, new StringBuilder(INITIAL_CAPACITY), pCompress);
   }

   /**
    * Ließt den wahlweisen komprimierten Inhalt der übergebenen Datei.
    *
    * @param pFile       Datei aus der gelesen wird
    * @param pCompressed {@code true:} die Daten werden aus einer komprimierten Datei gelesen, sonst
    *                    {@code false}.
    * @return Inhalt der Datei als {@link String}
    */
   @NotNull
   @Deprecated
   public static Optional<String> readStr(@NotNull File pFile, boolean pCompressed) {
      if (pCompressed) {
         return readStr(pFile);
      } else {
         return readCompressedStr(pFile);
      }
   }

   /**
    * Ließt den Inhalt der übergebenen Datei.
    *
    * @param pFile Datei aus der gelesen wird
    * @return Inhalt der Datei als {@link String}
    */
   @NotNull
   @Deprecated
   public static Optional<String> readStr(@NotNull File pFile) {
      try (BufferedReader br = new BufferedReader(
            new InputStreamReader(new FileInputStream(pFile), StandardCharsets.UTF_8))) {
         StringBuilder sb = new StringBuilder(INITIAL_CAPACITY);
         String line;

         while ((line = br.readLine()) != null) {
            sb.append(line).append(LINE_SEP);
         }

         br.close();
         return Optional.of(sb.toString());
      } catch (IOException pE) {
         MLog.log(pE);
      }
      return Optional.empty();
   }

   /**
    * Ließt den komprimierten Inhalt der übergebenen Datei.
    *
    * @param pFile Datei aus der gelesen wird
    * @return Inhalt der Datei als {@link String}
    */
   @NotNull
   public static Optional<String> readCompressedStr(@NotNull File pFile) {
      try (CBInputStream in = new CBInputStream(pFile)) {
         StringBuilder sb = new StringBuilder(INITIAL_CAPACITY);

         while (true) {
            try {
               sb.append(in.readChar());
            } catch (EOFException pE) {
               in.close();
               return Optional.of(sb.toString());
            }
         }
      } catch (IOException pE) {
         MLog.log(pE);
      }
      return Optional.empty();
   }

   /**
    * Ließt den wahlweisen komprimierten Inhalt übergebenen Datei und gibt diesen als {@link
    * ArrayList} zurück.
    *
    * @param pFile       Datei aus der gelesen wird
    * @param pCompressed {@code true:} die Daten werden aus einer komprimierten Datei gelesen, sonst
    *                    {@code false}.
    * @return Inhalt der Datei als {@link ArrayList}
    */
   @Deprecated
   @NotNull
   public static List<String> readList(@NotNull File pFile, boolean pCompressed) {
      if (pCompressed) {
         return readCompressedList(pFile);
      } else {
         return readList(pFile);
      }
   }

   /**
    * Ließt den komprimierten Inhalt der übergebenen Datei und gibt diesen als {@link ArrayList}
    * zurück.
    *
    * @param pFile Datei aus der gelesen wird
    * @return Inhalt der Datei als {@link ArrayList}
    */
   @Deprecated
   @NotNull
   public static List<String> readCompressedList(@NotNull File pFile) {
      try (CBInputStream in = new CBInputStream(pFile)) {
         List<String> list = new ArrayList<>(20);
         StringBuilder sb = new StringBuilder(INITIAL_CAPACITY);

         while (true) {
            try {
               char ch = in.readChar();
               if ((ch == '\r') || (ch == '\n')) {
                  list.add(sb.toString());
                  sb = new StringBuilder(128);
                  if (ch == '\r') {
                     ch = in.readChar();
                     if (ch != '\n') {
                        sb.append(ch);
                     }
                  }
               } else {
                  sb.append(ch);
               }
            } catch (EOFException pE) {
               in.close();
               return list;
            }
         }
      } catch (IOException pE) {
         MLog.log(pE);
      }
      return new LinkedList<>();
   }

   /**
    * Ließt den Inhalt der übergebenen Datei und gibt diesen als {@link ArrayList} zurück.
    *
    * @param pFile Datei aus der gelesen wird
    * @return Inhalt der Datei als {@link ArrayList}
    */
   @Deprecated
   @NotNull
   public static List<String> readList(@NotNull File pFile) {
      try (BufferedReader br = new BufferedReader(
            new InputStreamReader(new FileInputStream(pFile), StandardCharsets.UTF_8))) {
         List<String> list = new ArrayList<>(20);
         String line;

         while ((line = br.readLine()) != null) {
            list.add(line);
         }

         br.close();
         return list;
      } catch (IOException pE) {
         MLog.log(pE);
      }
      return new LinkedList<>();
   }

   /**
    * Kopiert die Datei an den übergebenen Pfad.
    *
    * @param pCopy  Pfad der Datei die kopiert wird
    * @param pPaste Pfad in den kopiert wird
    */
   @Deprecated
   public static void copyFile(@NotNull String pCopy, @NotNull String pPaste) {
      copyFile(new File(pCopy), new File(pPaste));
   }

   /**
    * Kopiert die Datei an den Pfad der übergebenen Datei.
    *
    * @param pCopy  Datei die kopiert wird
    * @param pPaste Datei in deren Pfad kopiert wird
    */
   @Deprecated
   public static void copyFile(@NotNull File pCopy, @NotNull File pPaste) {
      File folder = pPaste.getParentFile();
      if (!folder.exists()) {
         folder.mkdirs();
      }
      try (FileInputStream in = new FileInputStream(pCopy)) {
         try (FileOutputStream out = new FileOutputStream(pPaste)) {
            int bufferSize;
            byte[] buffer = new byte[1024];
            while ((bufferSize = in.read(buffer)) > 0) {
               out.write(buffer, 0, bufferSize);
            }
         }
      } catch (IOException pE) {
         MLog.log(pE);
      }
   }

   @Deprecated
   public static void delete(@NotNull File pFile) throws IOException {
      if (pFile.isDirectory()) {
         File[] files = pFile.listFiles();
         if (files != null) {
            for (File file : files) {
               delete(file);
            }
         }
      }
      Files.delete(pFile.toPath());
   }

   @Deprecated
   public static void listFiles(List<? super File> pFiles, File pFile) {
      listFiles(pFiles, pFile, null);
   }

   /**
    * Listet alle Dateien, die in dem übergebenen Ordner enthalten sind und fügt diese zur
    * übergebenen Liste hinzu.
    */
   @Deprecated
   public static void listFiles(List<? super File> pFiles, File pFile, FileFilter pFilter) {
      File[] list;
      if (pFilter == null) {
         list = pFile.listFiles();
      } else {
         list = pFile.listFiles(pFilter);
      }

      if (list == null) {
         return;
      }

      for (File file : list) {
         if (file.isDirectory()) {
            listFiles(pFiles, file, pFilter);
         } else {
            pFiles.add(file);
         }
      }
   }
}
